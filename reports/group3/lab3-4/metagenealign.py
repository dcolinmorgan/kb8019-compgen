# Program to join all the cluster files to form a metagene
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
import sys
dicto = {}
lists = ["clusterout1_outkalign.fasta","clusterout2_outkalign.fasta","clusterout3_outkalign.fasta","clusterout4_outkalign.fasta","clusterout5_outkalign.fasta","clusterout6_outkalign.fasta","clusterout7_outkalign.fasta","clusterout8_outkalign.fasta","clusterout9_outkalign.fasta","clusterout10_outkalign.fasta"]
seqtotal= ""
for files in lists:
	fileopen = open(files,'r')
	for seq_record in SeqIO.parse(fileopen, "fasta"):
		key1 = str(seq_record.id)                   # storing the title as the key
		sequence = str(seq_record.seq)              # storing the corresponding sequence of the above key
		if key1 in dicto:                           # Condition to check whether the key isi in dict or not
			dicto[key1].append(sequence)            # If yes then append the sequence to the already existing sequence for that particular key
		else:
			dicto[key1]=[sequence]                  # If no then create a new key with the new title and add the value as the corresponding sequence.
outfile= open("metagene_align.fasta",'w')              # Opens a new file to write the metagene
for i in dicto.keys():                              # For loop to read the keys and values in the dictionary
	outfile.write(">"+i+"\n"+''.join(dicto[i])+"\n")    # Joining the value list to get a concantenated sequence of all the sequences with the same species.
	print i,len(dicto[i])
outfile.close()
