# Script which adds all the orthologs into a same line.
#First joined all the ortholog files into a single file called ortholog_all.result
infile = open("ortholog_all.result",'r')
dicto1 = {}
for line in infile:                             # Reading each line word by word 
	words = line.strip().split()
	key1 = words[0]+"_"+words[1]                # Taking the words making up the key value that is the reffilename with specific protein
	value1 = '%s_%s' % (words[2], words[3])     # Taking up the value in the dictionary which are the orthologs of the above protein
	if key1 in dicto1:                          # Checking whether the key is there in the dictionary
		dicto1[key1].append(value1)             # If yes then append the values to the existing key which is actually the ortholog of that particular key.
	else:
		dicto1[key1] = [value1]                 # If the key is not there in the dict, then create a new key so the reference protein is added as a key with the existing ortholog.
	
for entry in dicto1:                            # Here we have the dict with reference protein names as the keys and the corresponding orthologs as the values in corresponding keys.
	if len(dicto1[entry])==8:                   # Selecting only the keys that have 8 orthologs
		#print entry, dicto1[entry]
		print '%s\t' %(entry), '\t'.join(dicto1[entry]) # Printing the keys and values by joining all the elements in the values of each key.
