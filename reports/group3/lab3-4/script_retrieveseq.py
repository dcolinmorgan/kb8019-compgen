# script to retrieve the sequences from the filename and protein title
infile = open("out_orthologs", 'r')
import sys
reffilename = "genome_0.results"
import os
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
import sys
j = 1
files = infile.readlines(10)
for num in range(10):                                           # Considering the first 10 lines
	path2= "clustersamename"+str(j)+".fasta"                
	outfile2= open(path2,'w')                                   # Opening the cluster file to write
	lines = files[num]
	words = lines.strip().split()
	refprotein = words[0].split('_')[2]                         # Defining the reference protein name
	openfile = open(reffilename, 'r')                           # Opening the reference file
	for seq_record in SeqIO.parse(openfile, "fasta"):           # Opens the reference file name and search for the required protein name
		if seq_record.id==refprotein:               
			outfile2.write(">species1"+"\n"+str(seq_record.seq)+"\n")# If it fins the protein name writes the title as well as the protein sequence
	for i in range(1,9):
		words1 = words[i].split('_')
		hitfilename = open('%s_%s.results' %(words1[0],words1[1]),'r')  # Opens the target protein file
		hitprotein = '_'.join(words1[2:])                               # Defines the name for the target protein
		for seq_record in SeqIO.parse(hitfilename, "fasta"):
			if seq_record.id==hitprotein:                               # Checks for the protein name in the target file
				outfile2.write(">species"+str(i+1)+"\n"+str(seq_record.seq)+"\n")   # If yes it writes the title as well as the sequence in fasta format
	j+=1	
	outfile2.close()
