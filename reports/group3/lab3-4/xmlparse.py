#Script for parsing the XML file from the blast output
from Bio.Blast import NCBIXML                       #Module for parsing the blast XML
import sys
refSpec = sys.argv[1]                               # Giving input arguments for reference species
tarSpec = sys.argv[2]                               # Giving input arguments for target species
blastFile = sys.argv[3]                             # Giving input arguments as the blast file
blast_records = NCBIXML.parse(open(blastFile,'r'))  # Opening the XML file
for record in blast_records:                        # REading each blast_records
	if len(record.descriptions)>0:                  # Checking whether it contains any description
		description = record.descriptions[0]        # If yes then print in this particular order and pipe it to a file to get all of the contents in a file.
		print refSpec+"_"+record.query+"\t"+tarSpec+"_"+(description.title).split()[1]