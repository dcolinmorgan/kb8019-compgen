# Script to parse the XML file from the Blastoutput
from Bio.Blast import NCBIXML
f = open("myfile.txt", "w")					    # Opens the file to write the output in fasta format
blast_records = NCBIXML.parse(open('output.blast'))		# Opens the xml file using the NCBIXML module to retrieve the data
blast_record = blast_records.next()				# blast_records contain all the data related to the particular proteome with all the hits.
str2 = ''
for alignment in blast_record.alignments:		# Taking the alignment from the blast_records
    for hsp in alignment.hsps:					# From the alignment taking the hsp from which we will get the title.
      str1 = alignment.title[16:]				# Assigning the title to the variable str1
      if str1 != str2: 						    # Since we need only the best hit we put a condition to check the title whether its repeating
        print '>', alignment.title[16:]			# If no then it prints the title and the sequence in fasta format 
        print hsp.sbjct
        f.write('>'+alignment.title[16:]+"\n")	# Writes the title and the sequence in fasta format 
        f.write('%s\n' % (hsp.sbjct,))
        str2 = str1						        # Also changes the second variable to the first one so the title does not repeat.
f.close()							            # Opened writing file closed
  