# Script to parse the hmmscn output
import sys
lists=[]
for i in sys.argv[1:]:				# Taking the argument files
	lists.append(i)
dictofaccall = {}				# Creating a dictionary to store the protein IDs and the frequency
outfile=open("see1",'w')
dict_genome = {}				# Dictionary which contains a dictionary as values to store all the protein IDs and their frequencies for individual proteomes
for files in lists:
	infile= open(files,'r')
	infile.readline()			# First 3 lines skipped
	infile.readline()
	infile.readline()
	dict_genome[files]={}			# Creating a dictionary for each proteome
	for lines in infile:
		words = lines.split()		# Splitting the lines to get the protein IDs
		if words[1] in dictofaccall:
			dictofaccall[words[1]]+=1	# If protein ids in dictionary then add to frequency. This is for the total
			
		else:
			dictofaccall[words[1]]=1	#If protein ids not in dictionary then create the id and add to frequency
			
		if words[1] in dict_genome[files]:	# If protein ids in dictionary then add to frequency. This is for the INDIVDUAL
			dict_genome[files][words[1]]+=1
		else:
			dict_genome[files][words[1]]=1
ranks={}						# Dictionary created to input the rank
for genomename in dict_genome:
	rank = 1
	ranks[genomename]={}				 # For each proteome created a dcitionary for ranking
	for w in sorted(dict_genome[genomename], key=dict_genome[genomename].get, reverse=True): # Condition to sort the values based on descending order
  		ranks[genomename][w]=rank		 # Storing to the dictionary with the rankings
		rank+=1
outfile.write("Domain_ID")
for genomenames in ranks:				# Writing from the dictionary
	outfile.write("\t"+genomenames)			# Writes the genome names (Here out0 means genome_0, out1 means genome_1)
outfile.write("\n------------------------------------------------------------------------------\n")
j=0
for domain in sorted(dictofaccall, key=dictofaccall.get, reverse=True):	# Condition to sort the values based on descending order. This time for the total proteins
	if j<10:							# Condition to get only the first 10 ranks
		outfile.write(domain)					# Writes the protein ids corresponding to the rank
		for genomes in ranks:					# Loop to print the ranks of the corresponding protein ID in all the proteomes
			if domain in ranks[genomes]:
				outfile.write("\t"+str(ranks[genomes][domain]))
			else:
				outfile.write("\tNA")			# If the protein Id does not exists it gives the result as NA
		outfile.write("\n")
	j+=1
#_____________________________________________________________________UNIQUE ID PROGRAM STARTS FROM HERE
uniquedict ={}						# Dictionary to store the all proteomes and no.of unique ids in each
for protids in dictofaccall:				# Each protein id from the total is taken
	unique = 0
	for genomes in ranks:
		if protids in ranks[genomes]:		# if id present in any of the genomes unique is incremented.
			unique+=1
			name = genomes
	if unique ==1:					# If unique is 1 then the protein id is unique and the variable 'name' has the proteome name in it
		if name in uniquedict:	
			uniquedict[name]+=1		# Here if the proteome name already present the number of unique proteins is added
		else:
			uniquedict[name]=1
outfile.write("\n----------------------------------\nProteomes\tno. of Unique Id's\n----------------------------------\n")
for key in uniquedict:					# lOOP TO PRINT OUT THE proteome name and the number of unique ids in each
	print key, uniquedict[key]
	outfile.write(key+"\t\t\t"+str(uniquedict[key])+"\n")
outfile.close()
