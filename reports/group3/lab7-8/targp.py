#Script to parse the targetp output
import sys
lists = []
for i in sys.argv[1:]:				# Take the arguments
	lists.append(i)
outfile = open("targetp.results",'w')		# To write to the output file
outfile.write('filename\tMT\tSP\n')
for files in lists:
	MT=0
	moreSPTM=0
	infile = open(files,'r')
	list_of_lines = infile.readlines()
	for line in list_of_lines[8:-2]:	# Loop to give all the lines in the file except the first 8 lines and the last 2 lines	
		#print line
		words = line.split()
		if (words[5])=='M':		# Condition to check the whether the its predicted as M or S if any of it then increment the corresponding variable
			MT+=1
			print words[0]
		elif (words[5])=='S':
			moreSPTM+=1
				
	outfile.write(files+"\t\t"+str(MT)+"\t"+str(moreSPTM)+"\n")	# Writes the values to the file.
outfile.close()
