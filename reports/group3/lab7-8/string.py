#The code to calculate the average connectivity.
#
#zcat /afs/pdc.kth.se/home/e/erison/home/Public/STRING_protein.links.v8.2.txt.gz| grep -w 10116  > checkr
#using the above code we extract the protien file coresponding to our bacterial and human.
import sys
inputfile = sys.argv[1]# we give the file get from the above code as an argument
dicstr={}              # we open dictionary to store the different protien and there frequency.
files = open(inputfile,'r')
length =0
for line in files:
	length+=1         #counting each line of the link protein 
    	words= line.split()
    	if words[0] in dicstr:    # it put the valaues in to the dictionary if that value exist add to the frequency
		dicstr[words[0]]+=1
	else:
		dicstr[words[0]]=1
average_connectivity=length/len(dicstr) # calculate the average connectivity
print average_connectivity


