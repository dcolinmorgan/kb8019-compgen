# script to parse the phobius output
import sys
lists = []
for i in sys.argv[1:]:						# Takes in the arguments
	lists.append(i)
outfile = open("phobius.results",'w')				# Opens the result file to write
outfile.write("filename\t\tTM=0\t\tTM>0\t\tAverageTM\tSP=0\t\tSP\n")
outfile.write("---------------------------------------------------------------------------------------------------\n")
for files in lists:
	TM=0
	moreTM=0
	avgTM=0
	SP=0
	moreSPTM=0
	infile = open(files,'r')				# Opens the output file
	infile.readline()					# first line ignored
	length = 0
	for line in infile:					
		length+=1
		#print line
		words = line.split()
		if float(words[1])==0:				# Condition to check the whether the TM is predicted as 0
			TM+=1
		elif float(words[1])>0:				# Condition to check the whether the TM is predicted as not zero and so what number
			moreTM+=1
			avgTM+=int(words[1])
		if (words[2])!="Y":				# Condition to check the whether the SP is predicted as Y
			#print "check"
			SP+=1
		elif (words[2])=="Y" and int(words[1])>0:	# Condition to check the whether the TM is predicted as non 0 and SP as Y
			moreSPTM+=1
	outfile.write(files+"\t"+str(float(TM)/length)+"\t"+str(float(moreTM)/length)+"\t"+str(avgTM)+"\t\t"+str(float(SP)/length)+"\t"+str(float(moreSPTM)/length)+"\n")				# Everything is taken fraction by dividing it with the whole length for each individual result.
outfile.close()
