# code to calculate the single and diaminoacid frequency
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
import sys
print "Output files are named as singleaminofreq.results & diaminofreq.results"
outfile1 = open("singleaminofreq.results",'w')						# Two files opened to write the frequencies for both seperately
outfile2 = open("diaminofreq.results",'w')
outfile1.write("----------------------------------------\nSingle aminoacid Frequency in percentage\n----------------------------------------\n")
outfile2.write("------------------------------------\nDiaminoacid Frequency in percentage\n------------------------------------\n")
first1 = True
first2 = True
print sys.argv[1:]
for filenames in sys.argv[1: ]:										# Main looop which will take all thhe argument files
	dicto1 = {}
	dicto2 = {}				
	outfile1.write("\n")
	outfile2.write("\n")
	path = "proteome_"+filenames[-1]
	totalseq =''
	for seq_record in SeqIO.parse(filenames, "fasta"):				# Reads the sequences from the proteome file and concantenate it
		#print "check"
		sequence = str(seq_record.seq)
		totalseq = totalseq + sequence
	
	length = len(totalseq)
	for i in range(length):											# Loop to check the frequency of the aminoacids and adds to the dictionary
		seq1 = totalseq[i]
		seq2 = totalseq[i:(i+2)]
		if seq1 in dicto1:
			dicto1[seq1] = dicto1[seq1]+1
		else:
			dicto1[seq1] = 1
		if len(seq2)==2:
			if dicto2.has_key(seq2):
				dicto2[seq2] = dicto2[seq2]+1
			else:
				dicto2[seq2] = 1
    keys1 = dicto1.keys()
	values1 = dicto1.values()
	total_value1 = 0
	for elem in values1:											# Loop to get the total count of frequencies
		total_value1 = total_value1+elem
	if first1 == True:
		outfile1.write("Filenames\t")
		first1 = False
		for elem in keys1:
			outfile1.write(elem+"\t")
		outfile1.write('\n')
	outfile1.write(filenames+'\t')
	for elem in keys1:												# Loop to write the frequencies to an output file
		key_freq1 = float(dicto1[elem])/total_value1*100
		outfile1.write(str(key_freq1)[:4]+"%\t")
     
    # To check the dinucleotide freq same as the same procedure above
	keys2 = dicto2.keys()
	values2 = dicto2.values()
	total_value2 = 0
	for elem in values2:
		total_value2 = total_value2+elem
	if first2 == True:
		outfile2.write("Filenames\t")
		first2 = False
		for elem in keys2:
			outfile2.write(elem+"\t")
		outfile2.write('\n')
	outfile2.write(filenames+'\t')
	for elem in keys2:
		key_freq2 = float(dicto2[elem])/total_value2*100
		outfile2.write(str(key_freq2)[:4]+"\t")
    
outfile1.close()													# Closing both the files
outfile2.close()
