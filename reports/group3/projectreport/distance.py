#! /usr/bin/env python				

import sys
import math
 
outputfilename = "distance.txt"
outputfile = open(outputfilename, "w")

# To read the input genome  fasta format file.
def readfile(filename):
	'''Print a file to the standard output.'''
	f = file(filename)
	string=''
	line = f.readline()
	if line[0] == ">":
		name = line[1:11]
                name = name.replace('\n','')
	else:
		sys.stderr.write('Please insert the arguments in fasta format files.\n')
		sys.exit()
	while True:
		line = f.readline()
		if len(line) == 0:
			break
                elif line[0]!= ">":
                    string = string+line    #the sequence is added to the variable string and later it is passed to the gccontent function
                
	f.close()
	gccontent_dinucleotide(string,name)
 	gccontent_nucleotide(string,name)       

# It calculate the GC percentage and the frequency of nuclotide
#and put it in to dictionary.

def gccontent_nucleotide(seq,name_key):
        l = seq
        A_count = l.count("A")              #Counts for A ,C,G,T in the string seq
        C_count = l.count("C")
        G_count = l.count("G")
        T_count = l.count("T")
        length = A_count+C_count+G_count+T_count   #Total length ie (A+G+C+T)
        #In below the frequency percentage for each nuclotide
	cg_A = float(A_count) / length*100
        cg_C = float(C_count) / length*100
        cg_G = float(G_count) / length*100
        cg_T = float(T_count) / length*100
        cg_percentage = float(C_count + G_count) / length*100    # GC percentage
        nucleotide_data = "%.2f" % cg_A,"%.2f" % cg_C,"%.2f" % cg_G,"%.2f" % cg_T
        dict_nucleotide[name_key]= nucleotide_data
        dict_cg_percentage[name_key]= str(cg_percentage)
        outputfile.write("GC percentage for"+" "+ name_key+ " " +str(cg_percentage) +"\n")

def gccontent_dinucleotide(seq,name_key):  
        l = seq
        AA_count = l.count("AA")              #Counts for di nucloitde in the string seq
        AC_count = l.count("AC")
        AG_count = l.count("AG")
        AT_count = l.count("AT")
        CA_count = l.count("CA")              
        CC_count = l.count("CC")
        CG_count = l.count("CG")
        CT_count = l.count("CT")
        GA_count = l.count("GA")              
        GC_count = l.count("GC")
        GG_count = l.count("GG")
        GT_count = l.count("GT")
        TA_count = l.count("TA")              
        TC_count = l.count("TC")
        TG_count = l.count("TG")
        TT_count = l.count("TT")
        length = (AA_count+AC_count+AG_count+AT_count+CA_count+CC_count+CG_count+CT_count +  
                 GA_count+GC_count+GG_count+GT_count+TA_count+TC_count+TG_count+TT_count)   #Total length ie for Dinuclotide
         #In below the frequency percentage for each dinuclotide
        cg_AA = float(AA_count) / length*100
        cg_AC = float(AC_count) / length*100
        cg_AG = float(AG_count) / length*100
        cg_AT = float(AT_count) / length*100
        cg_CA = float(CA_count) / length*100
        cg_CC = float(CC_count) / length*100
        cg_CG = float(CG_count) / length*100
        cg_CT = float(CT_count) / length*100
        cg_GA = float(GA_count) / length*100
        cg_GC = float(GC_count) / length*100
        cg_GG = float(GG_count) / length*100
        cg_GT = float(GT_count) / length*100
        cg_TA = float(TA_count) / length*100
        cg_TC = float(TC_count) / length*100
        cg_TG = float(TG_count) / length*100
        cg_TT = float(TT_count) / length*100
        dinucleotide_data = "%.2f" % cg_AA,"%.2f" % cg_AC,"%.2f" % cg_AG,"%.2f" % cg_AT, "%.2f" % cg_CA,"%.2f" % cg_CC,"%.2f" % cg_CG,"%.2f" % cg_CT,"%.2f" % cg_GA,"%.2f"% cg_GC,"%.2f" % cg_GG,"%.2f" % cg_GT,"%.2f" % cg_TA,"%.2f" % cg_TC,"%.2f" % cg_TG,"%.2f" % cg_TT
        dict_dinucleotide[name_key]= dinucleotide_data

# Distance matrix base on angle b/n frequency list for nuclotide
def angle_nucleotide(dic3):
    keys = dic3.keys()
    n = len(keys)
    outputfile.write("Inputfile No.for distance in angle nucleotide="+'%s' % n +"\n")
    print "Inputfile No.for distance in angle nucleotide="+str(n)
    angle_dic_nucleotide={}
    for i in range(0,n):
        string_nucleotide=[]
        for j in range(n):
                angle_distance=1000*-math.log10((1+(((float(dic3[keys[i]][0])* float(dic3[keys[j]][0]))  +
                      (float(dic3[keys[i]][1]) * float(dic3[keys[j]][1])) +
                      (float(dic3[keys[i]][2]) * float(dic3[keys[j]][2])) +
                      (float(dic3[keys[i]][3]) * float(dic3[keys[j]][3])))/
                      (math.sqrt(math.pow(float(dic3[keys[i]][0]),2) + math.pow(float(dic3[keys[i]][1]),2) +
                      math.pow(float(dic3[keys[i]][2]),2) + math.pow(float(dic3[keys[i]][3]), 2))*
		      math.sqrt(math.pow(float(dic3[keys[j]][0]),2) + math.pow(float(dic3[keys[j]][1]),2) +
                      math.pow(float(dic3[keys[j]][2]),2) + math.pow(float(dic3[keys[j]][3]),2))
                      )))/2)
                string_nucleotide.append("%.2f" % abs(angle_distance))
        angle_dic_nucleotide[keys[i]]=string_nucleotide
    for x in keys:
        print '%2s' % x,
        outputfile.write(x+" ")
    print 
    outputfile.write("\n")
    for x in keys:
        for values in angle_dic_nucleotide[x]:
            print '%7s' % values,
            outputfile.write('%9s' % values)
        print ""
        outputfile.write("\n")


#Distance matrix base on angle b/n frequency list for dinuclotide
def angle_dinucleotide(dic4):
    keys = dic4.keys()
    n = len(keys)
    outputfile.write("Inputfile No.for distance in angle dinuclotide="+'%s' % n +"\n")
    print "Inputfile No.for distance in angle dinuclotide="+str(n)
    angle_dic_dinucleotide={}
    for i in range(0,n):
        string_dinucleotide=[]
        for j in range(n):
               angle_distance=1000*-math.log10((1+(((float(dic4[keys[i]][0])*float(dic4[keys[j]][0]))+(float(dic4[keys[i]][1])*float(dic4[keys[j]][1]))+
                      (float(dic4[keys[i]][2]) * float(dic4[keys[j]][2]))  + (float(dic4[keys[i]][3]) * float(dic4[keys[j]][3]))+
		      (float(dic4[keys[i]][4]) * float(dic4[keys[j]][4]))  + (float(dic4[keys[i]][5]) * float(dic4[keys[j]][5])) +
                      (float(dic4[keys[i]][6]) * float(dic4[keys[j]][6]))  + (float(dic4[keys[i]][7]) * float(dic4[keys[j]][7])) +
		      (float(dic4[keys[i]][8]) * float(dic4[keys[j]][8]))  + (float(dic4[keys[i]][9]) * float(dic4[keys[j]][9])) +
                      (float(dic4[keys[i]][10]) * float(dic4[keys[j]][10])) +(float(dic4[keys[i]][11]) * float(dic4[keys[j]][11])) +
                      (float(dic4[keys[i]][12]) * float(dic4[keys[j]][12])) +(float(dic4[keys[i]][13]) * float(dic4[keys[j]][13])) +
                      (float(dic4[keys[i]][14]) * float(dic4[keys[j]][14])) +(float(dic4[keys[i]][15]) * float(dic4[keys[j]][15])))/
                    (math.sqrt(float(dic4[keys[i]][0])**2 + float(dic4[keys[i]][1])**2 + float(dic4[keys[i]][2])**2 + float(dic4[keys[i]][3])** 2+
                    float(dic4[keys[i]][4])**2 + float(dic4[keys[i]][5])**2 + float(dic4[keys[i]][6])**2 + float(dic4[keys[i]][7])** 2+
                    float(dic4[keys[i]][8])**2 + float(dic4[keys[i]][9])**2 + float(dic4[keys[i]][10])**2 + float(dic4[keys[i]][11])** 2+
                     float(dic4[keys[i]][12])**2 + float(dic4[keys[i]][13])**2 + float(dic4[keys[i]][14])**2 + float(dic4[keys[i]][15])** 2)*
                    math.sqrt(float(dic4[keys[j]][0])**2 + float(dic4[keys[j]][1])**2 + float(dic4[keys[j]][2])**2 + float(dic4[keys[j]][3])** 2+
                    float(dic4[keys[j]][4])**2 + float(dic4[keys[j]][5])**2 + float(dic4[keys[j]][6])**2 + float(dic4[keys[j]][7])** 2+
                    float(dic4[keys[j]][8])**2 + float(dic4[keys[j]][9])**2 + float(dic4[keys[j]][10])**2 + float(dic4[keys[j]][11])** 2+
                     float(dic4[keys[j]][12])**2 + float(dic4[keys[j]][13])**2 + float(dic4[keys[j]][14])**2 + float(dic4[keys[j]][15])** 2)
                    )))/2)
               string_dinucleotide.append("%.2f" % abs(angle_distance))
        angle_dic_dinucleotide[keys[i]]=string_dinucleotide
    for x in keys:
        print '%2s' % x,
        outputfile.write(x+" ")
    print 
    outputfile.write("\n")
    for x in keys:
        for values in angle_dic_dinucleotide[x]:
            print '%7s' % values,
            outputfile.write('%9s' % values)
        print ""
        outputfile.write("\n")

#Distance matrix base on GC Values.

def distance_cg_percentage(dic0):
    keys = dic0.keys()
    n = len(keys)
    outputfile.write("Inputfile No.for distance in cg_percentage="+'%s' % n )
    outputfile.write("\n")
    print "Inputfile No.for distance in cg_percentage="+str(n)
    gc_dic={}
    for i in range(0,n):
        string_cg=[]
        for j in range(n):
                gc_distance=float(math.sqrt(100*(
                    math.pow(float(dic0[keys[i]]) - float(dic0[keys[j]]),2))))
                string_cg.append("%.2f" % gc_distance)
        gc_dic[keys[i]]=string_cg
    for x in keys:
        print '%2s' % x,
        outputfile.write(x+" ")
    print 
    outputfile.write("\n")
    for x in keys:
        for values in gc_dic[x]:
            print '%7s' % values,
            outputfile.write('%9s' % values)
        print ""
        outputfile.write("\n")

#Distance matrix base on frequency list for nucleotide

def distance_nucleotide(dic1):
    keys = dic1.keys()
    n = len(keys)
    outputfile.write("Inputfile No.for distance in nucleotide="+'%s' % n +"\n")
    print "Inputfile No.for distance in nucleotide="+str(n)
    nucleotide_dic={}
    for i in range(0,n):
        string_nucleotide=[]
        for j in range(n):
                nucleotide_distance=math.sqrt(100*(
                  math.pow(float(dic1[keys[i]][0]) - float(dic1[keys[j]][0]), 2) +
                  math.pow(float(dic1[keys[i]][1]) - float(dic1[keys[j]][1]), 2) +
                  math.pow(float(dic1[keys[i]][2]) - float(dic1[keys[j]][2]), 2) +
                  math.pow(float(dic1[keys[i]][3]) - float(dic1[keys[j]][3]), 2)
                  ))
                string_nucleotide.append("%.2f" % nucleotide_distance)
        nucleotide_dic[keys[i]]=string_nucleotide
    for x in keys:
        print '%2s' % x,
        outputfile.write(x+" ")
    print 
    outputfile.write("\n")
    for x in keys:
        for values in nucleotide_dic[x]:
            print '%7s' % values,
            outputfile.write('%9s' % values)
        print ""
        outputfile.write("\n")
       
#Distance matrix base on frequency list for dinucleotide

def distance_dinucleotide(dic2):
    keys = dic2.keys()
    n = len(keys)
    outputfile.write("Inputfile No.for distance in dinucleotide="+'%s' % n +"\n" )
    print "Inputfile No. for distance in dinucleotide="+str(n)
    dinucleotide_dic={}
    for i in range(0,n):
        string_dinucleotide=[]
        for j in range(n):
                distance_dinucleotide=math.sqrt(100*(
                    math.pow(float(dic2[keys[i]][0]) - float(dic2[keys[j]][0]), 2) + math.pow(float(dic2[keys[i]][1]) - float(dic2[keys[j]][1]), 2) +
                    math.pow(float(dic2[keys[i]][2]) - float(dic2[keys[j]][2]), 2) + math.pow(float(dic2[keys[i]][3]) - float(dic2[keys[j]][3]), 2) +
                    math.pow(float(dic2[keys[i]][4]) - float(dic2[keys[j]][4]), 2) + math.pow(float(dic2[keys[i]][5]) - float(dic2[keys[j]][5]), 2) +
                    math.pow(float(dic2[keys[i]][6]) - float(dic2[keys[j]][6]), 2) + math.pow(float(dic2[keys[i]][7]) - float(dic2[keys[j]][7]), 2) +
                    math.pow(float(dic2[keys[i]][8]) - float(dic2[keys[j]][8]), 2) + math.pow(float(dic2[keys[i]][9]) - float(dic2[keys[j]][9]), 2) +
                    math.pow(float(dic2[keys[i]][10]) - float(dic2[keys[j]][10]), 2) + math.pow(float(dic2[keys[i]][11]) - float(dic2[keys[j]][11]), 2) +
                    math.pow(float(dic2[keys[i]][12]) - float(dic2[keys[j]][12]), 2) + math.pow(float(dic2[keys[i]][13]) - float(dic2[keys[j]][13]), 2) +
                    math.pow(float(dic2[keys[i]][14]) - float(dic2[keys[j]][14]), 2) + math.pow(float(dic2[keys[i]][15]) - float(dic2[keys[j]][15]), 2)
                    ))
                string_dinucleotide.append("%.2f" % distance_dinucleotide)
        dinucleotide_dic[keys[i]]=string_dinucleotide
    for x in keys:
        print '%2s' % x,
        outputfile.write(x+" ")
    print 
    outputfile.write("\n")
    for x in keys:
        for values in dinucleotide_dic[x]:
            print '%7s' % values,
            outputfile.write('%9s' % values)
        print ""
        outputfile.write("\n")

#______________________________________________ Script starts from here
if len(sys.argv) < 2:
	sys.stderr.write('Enter the file names for which you want to calculate the distance based on angle,nucleotide,dinucleotide frequencies and also based on GC values.\n') 
	sys.exit()

if sys.argv[1].startswith('--'):
	option = sys.argv[1][2:]
	# fetch sys.argv[1] but without the first two characters
	if option == 'version':
		print 'Version 1.1'
	elif option == 'help':
		print '''\
This program prints files to the standard output.
Any number of files can be specified.
Options include:
  --version : Prints the version number
  --help    : Display this help'''
	else:
		print 'Unknown option.'
	sys.exit()

else:
    dict_nucleotide={}
    dict_dinucleotide={}
    dict_cg_percentage={}
    for filename in sys.argv[1:]:
	    readfile(filename)
 #-------------------------------------------------------# Here we call all the diffiened function to calculate distance matrix.     
    distance_nucleotide(dict_nucleotide)
    distance_dinucleotide(dict_dinucleotide)
    distance_cg_percentage(dict_cg_percentage)
    angle_nucleotide(dict_nucleotide)
    angle_dinucleotide(dict_dinucleotide)
