# Program to find the ORf's and translates the genome to proteins.
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
import sys
import re
start = ["ATG","GTG","TTG"]									# Lists representing the start and the stop codons
stop = ["TAA","TAG","TGA"]
startcodons=0
atg=0
gtg=0
ttg=0
print "Output files are named proteome_*"
def startstoppost(nuc_sequence,direction,dicto2):			# Function to find the start and stop positon from the sequence argument
	global startcodons										# globalizing the variables
	global atg
	global gtg
	global ttg
	i = 0
	list1=[]
	for frames in range(3):
		check = "start"
		i=frames
		length = len(nuc_sequence)
		while i<(length-2):									# Loop to check the start and stop codons and avoids overlapping in the same frame
			codon = nuc_sequence[i:(i+3)]
			if (codon in start and check == "start"):
				startcodons+=1
				startpost = i
				check = "stop"
				if codon=="ATG":
					atg+=1
				if codon=="GTG":
					gtg+=1
				if codon=="TTG":
					ttg+=1
			elif (codon in stop and check == "stop"):
				stoppost = i+3
				dicto2[frames][direction][startpost]=stoppost
				check = "start"
			i = i+3
	print "\t\t",float(atg)/(atg+gtg+ttg)*100,"%\t",float(gtg)/(atg+gtg+ttg)*100,"%\t",float(ttg)/(atg+gtg+ttg)*100,"%"
	return(dicto2)
#Function to translate the argument sequence...............................................................................
def translateseq(trans_sequence):
	trans_seq = Seq(trans_sequence, IUPAC.unambiguous_dna)
	outfile.write(str(trans_seq.translate())[:-1])
	return(str(trans_seq.translate()))

#Function to retrieve the sequences based on the start and the stop position from the forward strand
def forward_strand(obj,for_seq):
	protein_id=1
	for frames in obj:
		for starts in obj[frames]['f']:
			dna_seq = for_seq[starts:obj[frames]['f'][starts]]
			outfile.write("\n>"+str(protein_id)+"\n")
			translateseq(dna_seq)
			protein_id+=1
	return(protein_id)
#Function to retrieve the sequences based on the start and the stop position from the reverse strand
def reverse_strand(obj,for_seq,prot_id):
	protein_id=prot_id
	for frames in obj:
		for starts in obj[frames]['r']:
			dna_seq = for_seq[starts:obj[frames]['r'][starts]]
			outfile.write("\n>"+str(protein_id)+"\n")
			translateseq(dna_seq)
			protein_id+=1

#def retreive_sequence()
#prog Starts here...................................................................................
lists = []
	
for elems in sys.argv[1: ]:
	lists.append(elems)
print lists
for filenames in lists:
	dicto1 = {}
	dicto1[0]={}
	dicto1[1]={}
	dicto1[2]={}
	dicto1[0]['f']={}
	dicto1[1]['f']={}
	dicto1[2]['f']={}
	dicto1[0]['r']={}
	dicto1[1]['r']={}
	dicto1[2]['r']={}
	sequence=""
	no_tatakeys = 0
	print no_tatakeys
	print "\n"+filenames
	print  "Startcodons\tATG\t\tGTG\t\tTTG"
	for seq_record in SeqIO.parse(filenames, "fasta"):					# Opening and reading the fasta files
		sequence1 = str(seq_record.seq)
		sequence = sequence1.replace("N","")
		dicto1 = startstoppost(sequence,"f",dicto1)						# taking the start and stop position of the forward strand
		my_seq = Seq(sequence, IUPAC.unambiguous_dna)
		complement = str(my_seq.reverse_complement())
		dicto1 = startstoppost(complement,"r",dicto1)					# taking the start and stop position of the reverse strand
	print "Number of ORF's before deleting the smaller ones\t",			#Printing the length before deleting the codons		
	print len(dicto1[0]['f'])+len(dicto1[0]['r'])+len(dicto1[1]['f'])+len(dicto1[1]['r'])+len(dicto1[2]['f'])+len(dicto1[2]['r'])
	for first in dicto1:												# Loop to read each keys in the dictionary to check the different restrictions.
		for second in dicto1[first]:
			del_keys = []
			tatakeys = []
			for keys in dicto1[first][second]:
				diff = dicto1[first][second][keys]-keys
				if diff < 90:											# Loop to exclude the shorter length sequences
					del_keys.append(keys)
				if second == 'f':										# Reads the keys in the forward frame
					elsecondition = True
					l1 = sequence[keys-15:keys-4]
					l2 = sequence[keys-40:keys-28]
					match1 = re.search(r'TA...T', l1)
					if match1:											# Checks for the promoter sequence
						tatakeys.append(keys)
						elsecondition = False
					match2 = re.search(r'TTG...', l2)
					if match2 and elsecondition:
						tatakeys.append(keys)
				if second == 'r':										# Reads the keys in the forward frame
					elsecondition = True
					l1 = complement[keys-15:keys-4]
					l2 = complement[keys-40:keys-28]
					match = re.search(r'TA...T', l1)
					if match:											# Checks for the promoter sequence
						tatakeys.append(keys)
						elsecondition = False
					match2 = re.search(r'TTG...', l2)
					if match2 and elsecondition:
						tatakeys.append(keys)
				if keys not in tatakeys:								# Storing the keys which should be excluded
						del_keys.append(keys)
				
			#deleting the keys from the dictionary which are stored in the del_keys
			for elems in del_keys:
				if elems in  dicto1[first][second]:
					del dicto1[first][second][elems]
			for elements in tatakeys:									# To calculate the number of ORF's predicted
				if elements in  dicto1[first][second]:
					no_tatakeys+=1
				
	print "Number of ORF's AFTER deleting the smaller ones\t\t",		# Printing the lengths after deleting uneccessary codons which r not coding.
	print len(dicto1[0]['f'])+len(dicto1[0]['r'])+len(dicto1[1]['f'])+len(dicto1[1]['r'])+len(dicto1[2]['f'])+len(dicto1[2]['r'])
	outfile = open ("proteome"+filenames[6:],'w')						# opening the file to write the protein sequences
	global_proteinid = 0
	global_proteinid = forward_strand(dicto1,sequence)					# Calling the forward function to retrieve the sequences
	reverse_strand(dicto1,complement,global_proteinid)
	print "no_tatakeys", no_tatakeys
	outfile.close()
