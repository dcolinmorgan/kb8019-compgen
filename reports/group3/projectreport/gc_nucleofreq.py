# Program to calculate the GC content as well as the frequencies of single and dinucleotides.
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
import sys
outfile1 = open("singlenucleo.results",'w')										# Creating two output files to write the results seperately
outfile2 = open("dinucleo.results",'w')
outfile1.write("----------------------------------------\nSingle nucleotide Frequency in percentage\n----------------------------------------\n")
outfile2.write("------------------------------------\nDinucleotide Frequency in percentage\n------------------------------------\n")
first1 = True
first2 = True
for path in sys.argv[1: ]:														# loop to take all the arguments.
	outfile1.write('\n')
	outfile2.write('\n')
	dicto1 = {}
	dicto2 = {}
	sequence=""
	for seq_record in SeqIO.parse(path, "fasta"):								# Opens the fasta file and takes in the sequence
		sequence1 = str(seq_record.seq)
		sequence = sequence1.replace("N","")									# We are removing the N's from the sequence
		length = len(sequence)
		for i in range(length):
		    seq1 = sequence[i]
		    seq2 = sequence[i:(i+2)]
		    if seq1 in dicto1:					
		        dicto1[seq1] = dicto1[seq1]+1
		    else:
		        dicto1[seq1] = 1
		    if len(seq2)==2:
		        if seq2 in dicto2:
		            dicto2[seq2] = dicto2[seq2]+1
		        else:
					dicto2[seq2] = 1
	#to check the GC content and single nnucleotide frequency..................................................................
	keys1 = dicto1.keys()
	values1 = dicto1.values()
	total_value1 = 0
	for elem in values1:														# To calculate the total value
	    total_value1 = total_value1+elem
	# to check the GC content 
	gc_content = float(dicto1["G"]+dicto1["C"])/total_value1*100
	print path, "\tGC content\t", str(gc_content)
	
	#Single nucleotide frequency..................................................................................................
	if first1 == True:
		outfile1.write("Filenames\t")
		first1 = False
		for elem in keys1:
			outfile1.write(elem+"\t")
		outfile1.write('\n--------------------------------------------\n')
	outfile1.write(path+'\t')
	for elem in keys1:														# Loop to write the frequencies to the output file
		key_freq1 = float(dicto1[elem])/total_value1*100
		outfile1.write(str(key_freq1)[:4]+"%\t")
     
    # To check the dinucleotide freq............................................................................................
	keys2 = dicto2.keys()
	values2 = dicto2.values()
	total_value2 = 0
	for elem in values2:													# To calculate the total value
		total_value2 = total_value2+elem
	if first2 == True:
		outfile2.write("Filenames\t")
		first2 = False
		for elem in keys2:													# Loop to write the frequencies to the output file
			outfile2.write(elem+"\t")
		outfile2.write('\n---------------------------------------------------------------------------------------------------------------------------------------------\n')
	outfile2.write(path+'\t')
	for elem in keys2:
		key_freq2 = float(dicto2[elem])/total_value2*100
		outfile2.write(str(key_freq2)[:4]+"\t")
    
outfile1.close()
outfile2.close()		
