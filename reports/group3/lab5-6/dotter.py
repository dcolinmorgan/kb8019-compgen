# Program to put unique sequence for the numbers in the cluster output file.
import random
import sys
file = open("orthoclusterprokaryotes.result",'r')		#Opened the cluster file to get the number of clusters
listlines = file.readlines()
length = len(listlines)						# Found the length
clusterid = {}
aminoacid = "ACDEFGHIKLMNPQRSTVWY"
checkrepeat = {}						# Dictionary initialised to check the duplicate
for i in range(length):
    sequence=""
    for j in range(20):
        sequence += random.choice(aminoacid)			# It creates the random amino acid sequence of length 20
    while sequence in checkrepeat:				# this loop checks for the duplicates of the sequence generated, if its a duplicate it creates another sequence
        sequence += random.choice(aminoacid)
    clusterid[i+1]=sequence					# Dictionary in which the keys are the cluster number and the values are the sequences
    checkrepeat[sequence]=i+1					# Dictionary to check the repeats so the keys are the protein identifiers
for filename in sys.argv[1:]:					# Opens all the files given as arguments
    fileopen = open(filename,'r')
    outfile = open(filename[:-8]+'.dotter','w')			# Writes into each output file with the protein name and the sequence in the fasta format
    words = fileopen.readline().split()
    outfile.write(">"+filename[:-8]+"\n")
    for number in words:
        outfile.write(clusterid[int(number)])			#Here the sequence is written to the file
    outfile.write("\n")
    outfile.close()
