# Program to identify the protein in which cluster and to arrange the proteome based on the geneorder.
import sys
proteomefile = sys.argv[1]					# INput file proteome
clusterfile = sys.argv[2]					# Input the clusterfile containing all the clusters.
proteinnames=[]
files = open(proteomefile,'r')
for line in files:						# Loop to create a list of the titles in proteinnames
    if line[0]==">":
        title = sys.argv[1]+"_"+line[1:].strip()
        proteinnames.append(title)
clusteropen1 = open(clusterfile,'r')				# Opening the cluster file
clusteropen = clusteropen1.readlines()
i = 1
clustertitles = {}
for line in clusteropen[:10]:					# Loop to put all the titles in to the dictionary as the key and the value as the cluster number.
    words = line.split()
    duplicate = False						# Variable to avoid the duplicates
    for word in words:
        if word in clustertitles:				#If the duplicate comes it breaks the loop.	
            duplicate = True
            break
    if not duplicate:						# If a new title comes it creates a new key with the value as the cluster number.
        for word in words:
            clustertitles[word] = i
        i+=1							# Iterator to iterate the cluster number.
print ">"+sys.argv[1]						# Prints the proteome name
for protein in proteinnames:					# Loop to print the cluster numbers in their order in the proteome file.
    if protein in clustertitles:
        print clustertitles[protein],
