#Script for parsing the XML file from the blast output
from Bio.Blast import NCBIXML                       	#Module for parsing the blast XML
import sys
outspec = sys.argv[2]                               	# Giving input arguments for creating the output file
blastFile = sys.argv[1]                            	# Give the blast file as argument  
blast_records = NCBIXML.parse(open(blastFile,'r'))  	# Used biopython to parse through the XML file to get the best hits.
outfile = open(outspec,'w')
for record in blast_records:
    if len(record.descriptions)>0:
        description = record.descriptions[0]
        outfile.write((description.title).split()[1]+'\n')# Writes only the protein Identifiers to a file
outfile.close()
