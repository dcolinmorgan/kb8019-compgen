#!/usr/bin/env python3

import sys

start_codon = ['ATG', 'GTG', 'TTG']
stop_codon = ['TGA', 'TAA', 'TAG']

dna_complement = {'A': 'T', 'T': 'A', 'G': 'C', 'C': 'G'}

# used for keeping track of current reading frame
class cc :
    def __init__(self) :
        self.counter = 0
    
    def increase(self) :
        if self.counter == 2 :
            self.counter = 0
        else :
            self.counter += 1

    def current(self) :
        return self.counter
        
# scan for orfs
def scan(sequence) :
    genes = list()
    # the three-step scanner
    counter = cc()
    curr_genes = [[0,0], [0,0], [0,0]]

    for i in range(len(sequence)) :
        if i + 2 > len(sequence) :
            break
        # gene start if start codon and no gene already started
        if sequence[i:i+3].upper() in start_codon and curr_genes[counter.current()][0] == 0 :
            curr_genes[counter.current()][0] = i+1
        # gene stop if stop codon and no gene already started
        if sequence[i:i+3].upper() in stop_codon and curr_genes[counter.current()][0] != 0 :
            curr_genes[counter.current()][1] = i+1
            # protein length of at least 28 means that less than 1% of all genes will be missed
            if (curr_genes[counter.current()][1] - curr_genes[counter.current()][0]) // 3 > 28 :
                genes.append(curr_genes[counter.current()])
            curr_genes[counter.current()] = [0,0]
        
        counter.increase()
        
    return genes

def rm_overlaps(genes) :
    # part of an attempt of optimisation; won't have to restart calculation every time
    last = None
    identical = False
    full_run = False
    full_done = False
    while not identical :
        # run until a full run gives no more changes
        identical = True
        # if no confirmed stable point
        if last is None :
            range_s = 0
        else :
            # cant find the reason for this bug; last should always be remaining in genes[],
            # but for genome 4 and 7, it says it's in some cases not.
            # just doing the following exception handling handles it at least.
            try :
                range_s = genes.index(last)
            except ValueError :
                range_s = 0
        for i in range(range_s, len(genes)) :
            # another protection thingie
            if i >= len(genes) :
                break
            # if outer for loop had no deletions
            survived = True
            try :
                start = genes[i][0]
                stop = genes[i][1]
            except IndexError :
                print(i, len(genes))
            # check five closest orfs for overlaps
            for j in range(i+1, i+6) :
                if j >= len(genes) :
                    break
                # if the ranges of the genes overlap
                if ((genes[j][0] >= start and genes[j][0] <= stop) or
                    (genes[j][1] >= start and genes[j][1] <= stop) or
                    (genes[j][0] <= start and genes[j][1] >= stop)) :
                    # get rid of gene with shortest length
                    if stop - start > genes[j][1] - genes[j][0] :
                        genes.remove(genes[j])
                        identical = False
                    else :
                        # if gene from main loop is removed, the loop better be restarted
                        genes.remove(genes[i])
                        identical = False
                        survived = False
                        break
            # if gene from main loop is removed, the loop should better be restarted, save last stable to avoid full restart
            if not survived :
                if i-1 > 0 :
                    last = genes[i-1]
                break
            if i == len(genes) - 1 :
                full_run = True

        # keep on running until entire thing is unchanged
        if not identical :
            full_done = False

        # to force a full run-through at the end to get rid of possible new overlaps
        if not full_done and full_run:
            full_done = True
            identical = False
            last = None
    return genes

def rm_overlaps_full(genes_c, genes_r) :
    # similar to rm_overlaps, but with two for loops, one per strand
    # to merge and remove overlaps for the gene positions of reverse and original strand
    identical = False
    last = None
    full_run = False
    full_done = False
    while not identical :
        identical = True
        # if no confirmed stable point
        if last is None :
            range_s = 0
        else :
            range_s = genes_c.index(last)
        for i_c in range(range_s, len(genes_c)) :
            survived = True
            for i_r in range(len(genes_r)-1, -1, -1) :
                if ((genes_r[i_r][0] >= genes_c[i_c][0] and genes_r[i_r][0] <= genes_c[i_c][1]) or
                    (genes_r[i_r][1] >= genes_c[i_c][0] and genes_r[i_r][1] <= genes_c[i_c][1]) or
                    (genes_r[i_r][1] <= genes_c[i_c][0] and genes_r[i_r][0] >= genes_c[i_c][1])) :
                    if genes_c[i_c][1] - genes_c[i_c][0] > genes_r[i_r][0] - genes_r[i_r][1] :
                        genes_r.remove(genes_r[i_r])
                        identical = False
                        break
                    else :
                        genes_c.remove(genes_c[i_c])
                        survived = False
                        identical = False
                        break
            if not survived :
                if i_c -1 > 0 :
                    last = genes_c[i_c-1]
                break 
            if i_c == len(genes_c) - 1 :
                full_run = True
            
        # keep on running until entire thing is unchanged
        if not identical :
            full_done = False

        # to force a full run-through at the end to get rid of possible new overlaps
        if not full_done and full_run:
            full_done = True
            identical = False
            last = None
    return genes_c + genes_r
    
# generate the reverse strand
def comp_strand(sequence) :
    new_seq = ''
    for i in sequence[::-1] :
        new_seq += dna_complement[i]
    return new_seq

def comp_numbering(genes, seq_length) :
    # change numbering of reverse strand to that of the "original" strand
    for gene in genes :
        gene[0] = seq_length - gene[0] + 1
        gene[1] = seq_length - gene[1] + 1
    return genes

if len(sys.argv) != 2 :
    sys.stderr.write('Usage: {}  <FASTA file>\n'.format(sys.argv[0]))
    sys.exit()

# read sequences
SEQUENCE_FILE = open(sys.argv[1]).read()
header, sequence=SEQUENCE_FILE.split('\n',1)
sequence = sequence.strip()
sequence = sequence.replace('\n','')

# find all ORFs in 'original' strand
orf_o = scan(sequence)
orf_o = rm_overlaps(orf_o)

# find all ORFs in reverse strand
orf_r = scan(comp_strand(sequence))
orf_r = rm_overlaps(orf_r)

# fix numbering of reverse strand genes
orf_r = comp_numbering(orf_r, len(sequence))

# remove overlaps between the strands
orf = rm_overlaps_full(orf_o, orf_r)

# print results
print('Name\tStart\tEnd')
counter = 1
for gene in orf :
    print('{}\t{}\t{}'.format('orf'+str(counter), gene[0], gene[1]))
    counter += 1
