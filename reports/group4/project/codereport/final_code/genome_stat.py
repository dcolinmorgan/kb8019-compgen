#!/usr/bin/python3

import sys
import os

# for nucleotides statistics
def nucleotides(headers, sequences, count_ambig, short_out) :

    nucleotide = {'A': 0, 'T': 1, 'U': 1, 'G': 2, 'C': 3, 'S': 5, 'W': 6, 'N': 4, 
                  'M': 4, 'R': 4, 'Y': 4, 'K': 4, 'V': 4, 'H': 4, 'D': 4, 'B': 4}
    nuc = list()
    for i in range(len(nucleotide)) :
        nuc.append(0)
        
    # dinucleotides
    dinucleotide = { 'AA': 0, 'AT': 1, 'AG': 2, 'AC': 3,
                     'TT': 4, 'TA': 5, 'TG': 6, 'TC': 7,
                     'GG': 8, 'GA': 9, 'GT': 10, 'GC': 11,
                     'CC': 12, 'CA': 13, 'CT': 14, 'CG': 15}
    dinuc = list()
    for i in range(len(dinucleotide)) :
        dinuc.append(0)

    last = None

    for s in range(len(sequences)) :
        bad_sequence = False
        bad_sequence_di = False
        # nuc/dinuc in different loops to allow for better error handling
        for i in sequences[s] :
            try :
                nuc[nucleotide[i.upper()]] += 1
            except KeyError :
                sys.stderr.write('E: The sequence \'{}\' contains unknown nucleotide \'{}\'\n'.format(headers[s][1:], i))
                bad_sequence = True
                break

        if bad_sequence :
            continue
            
        for i in sequences[s] :
            if i == 'N' :
                last = None
                continue
            if last == None :
                last = i
                continue 
            try :
                dinuc[dinucleotide[(last + i).upper()]] += 1
            except KeyError :
                error = 'E: The sequence \'{}\' contains unknown dinucleotide \'{}\'\n'.format(headers[s][1:], last +i)
                bad_sequence_di = True
                break
            last = i

        # sequence statistics
        print(headers[s][1:] + ':')
        # gc content        
        if count_ambig :
            totnuc = 0
            for i in nuc :
                totnuc += i
            if short_out :
                print(round((nuc[nucleotide['G']] + nuc[nucleotide['C']] + nuc[nucleotide['S']]) / totnuc, 4))
            else :
                print('GC content: ' + '%.4f' % float((nuc[nucleotide['G']] + nuc[nucleotide['C']] + nuc[nucleotide['S']]) / totnuc))
        
        totnuc = 0
        for i in nuc[:5] :
            totnuc += i
        if not count_ambig :
            if short_out: 
                print(round((nuc[nucleotide['G']] + nuc[nucleotide['C']]) / totnuc, 4))
            else :
                print('GC content: ' + '%.4f' % float((nuc[nucleotide['G']] + nuc[nucleotide['C']]) / totnuc))

        totdinuc = 0    
        for i in dinuc :
            totdinuc += i

    
        # nucleotide fractions
        if not short_out :
            print('A\tT\tG\tC')
        print('{0}\t{1}\t{2}\t{3}'.format(round(nuc[nucleotide['A']] / totnuc, 4),
                                          round(nuc[nucleotide['T']] / totnuc, 4),
                                          round(nuc[nucleotide['G']] / totnuc, 4), 
                                          round(nuc[nucleotide['C']] / totnuc, 4)))
        if bad_sequence_di :
            sys.stderr.write(error)
            continue
        # dinucleotide fractions:
        if not short_out :
            print('AA\tAT\tAG\tAC\tTT\tTA\tTG\tTC\nGG\tGA\tGT\tGC\tCC\tCA\tCT\tCG')
        
        counter = 0
        out_string = ''
        for i in dinuc :
            out_string += str(round(i/totnuc, 4))
            if counter == 7 :
                out_string += '\n'
            else :
                out_string += '\t'
            counter += 1

        print(out_string)
               
def residues(headers, sequence, short_out) :
    aa = {'A': 0, 'C': 1, 'D': 2, 'E': 3, 'F': 4, 'G': 5, 'H': 6, 'I': 7, 'K': 8, 'L': 9,
      'M': 10, 'N': 11, 'P': 12, 'Q': 13, 'R': 14, 'S': 15, 'T': 16, 'V': 17, 'W': 18, 'Y': 19, 'X': 20, '*': 20}

    res = list()
    for i in range(len(aa)) :
        res.append(0)

    if len(sequences) > 0 and not short_out:
        print('A\tC\tD\tE\tF\tG\tH\tI\tK\tL\nM\tN\tP\tQ\tR\tS\tT\tV\tW\tY')

    for s in range(len(sequences)) :
        for r in sequences[s] :
            bad_sequence = False
            try :
                res[aa[r.upper()]] += 1
            except KeyError :
                sys.stderr.write('E: The sequence \'{}\' contains unknown residue \'{}\'\n'.format(headers[s][1:], r))
                bad_sequence = True
                break
        if bad_sequence :
            continue

        tot_res = 0
        for i in res[:-2] :
            tot_res += i

        counter = 0
        out_string = ''
        for i in res[:-2] :
            out_string += str(round(i/tot_res, 4))
            if counter == 9 :
                out_string += '\n'
            else :
                out_string += '\t'
            counter += 1

        print(headers[s][1:] + ':')
        print(out_string)
        if not short_out :
            if res[-1] + res[-2] > 0 :
                print('Undetermined (X/*) residues: {}'.format(res[-1] + res[-2]))

# help
if len(sys.argv) == 1 or '-h' in sys.argv :
    sys.stderr.write('Usage: {0} -f <fasta file> [-a n/y] [-t n/p] [-s n/y]\n'.format(sys.argv[0]) + 
                     '-a count ambigous nucleotides for gc content calculation - n default\n' +
                     '-t protein/nucleotide sequence - n default\n' +
                     '-s short output - n default\n')
    sys.exit()

# basic error checking/setting of variables
fileName = ''
if '-f' in sys.argv :
    if os.path.isfile(sys.argv[sys.argv.index('-f') + 1]) :
        fileName = sys.argv[sys.argv.index('-f') + 1]
    else :
        sys.stderr.write('E: no such file {0}\n'.format(sys.argv[sys.argv.index('-f') + 1]))
        sys.exit()
else :
    sys.stderr.write('E: need an input file\n')
    sys.exit()

count_ambig = False
if '-a' in sys.argv :
    try :
        if sys.argv[sys.argv.index('-a') + 1].lower() == 'y' :
            count_ambig = True
    except IndexError :
        sys.stderr.write('E: no -a parameter - use n or y\n')
        sys.exit()        
    if not (sys.argv[sys.argv.index('-a') + 1].lower() == 'n' or sys.argv[sys.argv.index('-a') + 1].lower() == 'y') :
        sys.stderr.write('E: incorrect -a parameter - use n or y\n')
        sys.exit()

prot_seq = False
if '-t' in sys.argv :
    try :
        if sys.argv[sys.argv.index('-t') + 1].lower() == 'p' :
            prot_seq = True
    except IndexError :
        sys.stderr.write('E: no -t parameter - use n or p\n')
        sys.exit()
    if not (sys.argv[sys.argv.index('-t') + 1].lower() == 'n' or sys.argv[sys.argv.index('-t') + 1].lower() == 'p') :
        sys.stderr.write('E: incorrect -t parameter - use n or p\n')
        sys.exit()

short_out = False
if '-s' in sys.argv :
    try :
        if sys.argv[sys.argv.index('-s') + 1].lower() == 'y' :
            short_out = True
    except IndexError :
        sys.stderr.write('E: no -s parameter - use n or y\n')
        sys.exit()        
    if not (sys.argv[sys.argv.index('-s') + 1].lower() == 'n' or sys.argv[sys.argv.index('-s') + 1].lower() == 'y') :
        sys.stderr.write('E: incorrect -s parameter - use n or y\n')
        sys.exit()

# read sequences
infile = open(fileName)

headers = list()
sequences = list()

tmp = infile.readline()
if tmp[0] == '>' :
        headers.append(tmp[:-1])
        sequences.append('')
else :
    sys.stderr.write('E: file does not start with >\n')

for line in infile :
    if line[0] == '>' :
        headers.append(line[:-1])
        sequences.append('')
    else :
        # sequence continued
        sequences[-1] += line[:-1]

# get rid of whitespace
for i in range(len(sequences)) :
    sequences[i] = sequences[i].replace(' ', '')

if not prot_seq :
    nucleotides(headers, sequences, count_ambig, short_out)
else :
    residues(headers, sequences, short_out)
