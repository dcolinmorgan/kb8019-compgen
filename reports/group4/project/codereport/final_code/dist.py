#!/usr/bin/env python3

import sys
import math
import os

def dist_gc(statistics, headers, scale) :
    # create 2D table
    table = list()

    for i in range(len(statistics)) :
        table.append(list())
        for j in range(len(statistics)) :
            table[-1].append(math.sqrt((statistics[i][0]-statistics[j][0])**2))

    print_results(table, headers, scale)

def dist_nt(statistics, headers, scale) :
    table = list()

    for i in range(len(statistics)) :
        table.append(list())
        for j in range(len(statistics)) :
            tmp = 0
            for k in range(4) :
                tmp += (statistics[i][k]-statistics[j][k])**2
            table[-1].append(math.sqrt(tmp))

    print_results(table, headers, scale)

def dist_dt(statistics, headers, scale) :
    table = list()

    for i in range(len(statistics)) :
        table.append(list())
        for j in range(len(statistics)) :
            tmp = 0
            for k in range(16) :
                tmp += (statistics[i][k]-statistics[j][k])**2
            table[-1].append(math.sqrt(tmp))

    print_results(table, headers, scale)

def dist_all(statistics, headers, scale) :
    table = list()

    for i in range(len(statistics)) :
        table.append(list())
        for j in range(len(statistics)) :
            tmp = 0
            # gc + nt + dt = 21 entries
            for k in range(21) :
                tmp += (statistics[i][k]-statistics[j][k])**2
            table[-1].append(math.sqrt(tmp))

    print_results(table, headers, scale)
    

def print_results(table, headers, scale) :
    if scale :
        longest_dist = 0
        for i in range(len(table)) :
            for j in range(len(table)) :
                if table[i][j] > longest_dist :
                    longest_dist = table[i][j]

        scale_factor = 100/longest_dist


        for i in range(len(table)) :
            for j in range(len(table)) :
                table[i][j] *= scale_factor

    output = ''
    for header in headers :
        output += header + '\t'
    print(output)

    for i in range(len(table)) :
        output = ''
        for j in range(len(table)) :
            output += str(round(table[i][j], 4)) + '\t'
        print(output)

# help
if len(sys.argv) == 1 or '-h' in sys.argv :
    sys.stderr.write('Usage: {0} -f <genome_stat short output file> [-t gc/nt/dt] [-s y/n]\n'.format(sys.argv[0]) + 
                     '-t type of distance calculation; gc, nucleotide (nt), dinucleotide (dt) or all (all)\n' +
                     '-s scale the results for usage in belvu y/n - y default\n')
    sys.exit()

# checking parameters
fileName = ''
if '-f' in sys.argv :
    if os.path.isfile(sys.argv[sys.argv.index('-f') + 1]) :
        fileName = sys.argv[sys.argv.index('-f') + 1]
    else :
        sys.stderr.write('E: no such file {0}\n'.format(sys.argv[sys.argv.index('-f') + 1]))
        sys.exit()
else :
    sys.stderr.write('E: need an input file\n')
    sys.exit()

if '-t' in sys.argv :
    try :
        calculation = sys.argv[sys.argv.index('-t') + 1 ]
    except IndexError :
        sys.stderr.write('E: no -t parameter - use gc, nt, dt or all\n')
        sys.exit()        
    if calculation.lower() not in ['gc', 'nt', 'dt', 'all'] :
        sys.stderr.write('E: incorrect -t parameter - use gc, nt, dt or all\n')
        sys.exit()

scale = True
if '-s' in sys.argv :
    try :
        if sys.argv[sys.argv.index('-s') + 1].lower() == 'n' :
            scale = False
    except IndexError :
        sys.stderr.write('E: no -s parameter - use y or n\n')
        sys.exit()        
    if sys.argv[sys.argv.index('-s') + 1].lower() not in ['y', 'n'] :
        sys.stderr.write('E: incorrect -s parameter - use y or n\n')
        sys.exit()

# read stats
infile = open(fileName, 'r')

statistics = list()
headers = list()

counter = 0
for line in infile :
    if ':' in line :
        if calculation in ['dt', 'all'] and counter not in [0, 5] :
            sys.stderr.write('E: one or more entries do not contain dt data\n')
            sys.exit()
        counter = 0
        statistics.append(list())
        headers.append(line[:-2]) # get rid of : and \n
    if counter == 1 and calculation in ['gc', 'all']:
        # gc, one field on line
        statistics[-1].append(float(line[:-1]))
    elif counter == 2 and calculation in ['nt', 'all'] :
        # nt, four fields, \t inbetween
        columns = line[:-1].split('\t')
        for i in range(4) :
            statistics[-1].append(float(columns[i]))
    elif counter in [3,4] and calculation in ['dt', 'all'] :
        # dt, eight fields, two lines, \t inbetween
        columns = line[:-1].split('\t')
        for i in range(8) :
            statistics[-1].append(float(columns[i]))
    counter += 1

if calculation == 'gc' :
    dist_gc(statistics, headers, scale)
elif calculation == 'nt' :
    dist_nt(statistics, headers, scale)
elif calculation == 'dt' :
    dist_dt(statistics, headers, scale)
elif calculation == 'all' :
    dist_all(statistics, headers, scale)
