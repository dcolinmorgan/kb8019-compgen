#script to create Ortholog cluster from different proteins  

import os			# module helps to read all files from folder 

w = open('simpleallrows.txt', 'w+a')
path = "/home/praveen/Downloads/allprts/"
files = os.listdir(path)                     # First we want to extract the ref id's and isolate each one of them.

a = open('refnhitID_prot2', 'r')	    
b = a.readlines()

i = 0
g = ''

while i < len(b):
	s = str(b[i]).split()		# splits the word to use this name to search 
	d = s[0]

	for inFile in files:
		f = file(path+inFile,'r')	
		for line in f:
			h = line.split()	
			if d == h[0]:
				g = g +' '+ h[1]
	
	w.write(d+' '+g+'\n'+str(i)+'\n\n')		# writes to file in a all species cluster in a single row 
	g = ''
	i = i + 1
