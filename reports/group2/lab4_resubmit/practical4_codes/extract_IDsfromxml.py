# script to read query protein ID and hit protein ID  and writes in two columns

from Bio.Blast import NCBIXML     		# module to read XML files
import sys

w = open('refnhitID_sample', 'w+a')

result_handle = open(sys.argv[1])	
query_tag = sys.argv[2]			# read species name of the ref protein from command line 
hit_tag = sys.argv[3]			# read species name of the hit protein from command line 

records = NCBIXML.parse(result_handle)  # parse the required elements from the XML file
i = 0

for record in records:
    if record.alignments:
        #print query_tag +"_"+ record.query + "\t" + hit_tag +"_"+record.alignments[0].title.split(" ")[1] 
        w.write(str(query_tag +"_"+ record.query + "\t" + hit_tag +"_"+record.alignments[0].title.split(" ")[1])+"\n") 
           


# parse = allows you to retrieving BLAST records one by one for each BLAST search result
# Note though that you can step through the BLAST records only once






