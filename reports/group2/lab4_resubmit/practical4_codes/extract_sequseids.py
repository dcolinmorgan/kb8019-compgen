# script to extract sequences from multiprotein fasta files based on their ID's

import os			# module to open direct
from Bio import SeqIO
from Bio.Seq import Seq		# Biopython module to read fasta formated files 



path = "/home/praveen/Downloads/protfile/"
files = os.listdir(path)

a = open('10rowIDall.txt', 'r')	
b = a.readlines()

i=0

while i<len(b):
    c= b[i].split()
    w = open('cluster'+str(i+1),'w')					# writes into 10 protein cluster files 
    for wd in c:
        d=str(wd).split('_')
        
        f = file(path+d[0],'r')
        for seq_record in SeqIO.parse(f, "fasta"):    
                if seq_record.id==d[1]:					#condition which checks the protein Ids in both files and wites
                    
                    #print (">"+seq_record.id+'\n'+seq_record.seq+'\n')
                    w.write(str(">"+d[0]+'\n'+seq_record.seq+'\n'))
    w.close()
    i+=1
    

