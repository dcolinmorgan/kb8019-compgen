#Script to calculate each nucleaotide Frequency_percent,Dinuleotide Frequency_percent and GC content 

from Bio import SeqIO  # Imported bio function to read fasta file
import os              # function to read from folder


# Function for create dictionary for dinucleotide count, total number of base count and each base count
def freq(e):            
  nLst   = ['A','C','G','T','N']
  lst = {} 
  lst['A']=[]
  lst['C']=[]
  lst['G']=[] 
  lst['T']=[]
  lst['N']=[]
  e = e.upper()                                               #converts to upper case 
  nCnt= {}           
  dinCnt= {} 
  for p in nLst:
    nCnt[p]=0
    dinCnt[p]={}
    for q in nLst:
      dinCnt[p][q]=0    
  nCnt[e[0]]= 1
  ntotal= 0
  for j in range(len(e)-1): 
    p = e[j]
    q = e[j+1]
    lst[p].append(q)                                          # addingup to list   
    dinCnt[p][q]+=1                                           #counts every added dinucleotides 
    ntotal+=1
    nCnt[q]+=1
  return dinCnt,ntotal,nCnt    

path = "/home/praveen/Desktop/dinucleotide_new_frq/results/"  #target species file name    
lists = os.listdir(path)
for files in lists:                                              # reading from the dict and output writes to new file for each input file                                   
    fileopen = open(path+files,'r')     
    outfl=open("Di_nuclfreq_out"+'_'+files,'w')
    for record in SeqIO.parse(fileopen,'fasta'):
        seq_id = '';seq = ''
        seq_id += record.id;seq += record.seq
    freq(seq)
    k=[]
    h=freq(seq)[0]
    c=freq(seq)[1]
    n=freq(seq)[2]
    nlA_freq_perc =float(float(n['A'])/float(c))*100            #calculates frequency and percent
    nlC_freq_perc =float(float(n['C'])/float(c))*100
    nlT_freq_perc =float(float(n['T'])/float(c))*100
    nlG_freq_perc =float(float(n['G'])/float(c))*100
    print 'A_freq_percentage:',nlA_freq_perc,'\n','C_freq_percentage:',nlC_freq_perc,'\n','T_freq_percentage:',nlT_freq_perc,'\n','G_freq_percentage:',nlG_freq_perc
    outfl.write(str('A_freq_percentage:')+str(nlA_freq_perc)+'\n')
    outfl.write(str('C_freq_percentage:')+str(nlC_freq_perc)+'\n')
    outfl.write(str('T_freq_percentage:')+str(nlT_freq_perc)+'\n')
    outfl.write(str('G_freq_percentage:')+str(nlG_freq_perc)+'\n')
    
    #Function to add all dinucleotide count and finds the frequncy of dinuleotide_percent 
    def to_sum(ti):        
        difreq_per =[]
        sumDigits = 0
        for c in ti:
            sumDigits += int(c)
        for frdi in ti:
            difreq_per.append(float(frdi)/float(sumDigits)*100)
        return sumDigits,difreq_per
    
    totdi=[]
    totdi_nucl=[]
    for f in h:             
        for k in h[f]:
            nu2=[f]
            nu2.append(k)
            on2=''.join(nu2)
            totdi_nucl.append(on2)
            totdi.append(float(h[f][k]))
        length = n['A']+n['C']+n['G']+n['T']                        #Total length ie (A+G+C+T)
        gc_percentage = float(n['G']+n['C'])/length*100             #calculates GC percentage
    to_sum(totdi)
    for frech in  range(len(totdi_nucl)):                           #calling function to write all freq to a new file 
        print 'Dinucleotide_freq_percentages_of:',totdi_nucl[frech],to_sum(totdi)[1][frech]
        outfl.write(str('Dinucleotide_freq_percentages_of:'))
        outfl.write(str(totdi_nucl[frech])+' ')
        outfl.write(str(to_sum(totdi)[1][frech])+'\n')
    outfl.write(str("GC_percentage:")+str(gc_percentage)+'\n'+str(n)+'\n')   
    print "GC_percentage:",gc_percentage

outfl.close() 
