#script to Find Open reading frames(Orf's) from genome file which is in fasta format

from Bio import SeqIO                                   # Imports biopyhton function to read fasta format seq
import sys                                              # Function to import from terminal 


start='ATG'                                             #we consider only one start codon. because it has more occurrence percentage
stop1 ="TAA";stop2="TGA";stop3="TAG"

#a is the test sequnce 
#a ="ATGCTAATGAGTATCGCGATGCGCGAGCAGTTAAGATGATAGTATGTAAGTAA" 

file1= open(sys.argv[1],'r')                            #Input fasta format file  
for record in SeqIO.parse(file1,'fasta'):
    seq_id = '';seq = ''
    seq += record.seq
    a=''.join(seq)
    
#This cleanup function is to delete all unwanted empty spaced in list.
#we can say empty space in the list
def cleanup(lines):              
    w=[]
    for j in lines:
        if j =='':
            del j
        else:
            w.append(j)
    return w
        
i=0;
f1=[];f2=[];f3=[]
for j in (a):
    frm1=a[i:(i+3)]                         
    frm2=a[(i+1):(i+4)]                     
    frm3=a[(i+2):(i+5)]                     
    i =i+3 
    f1.append(frm1)                            
    f2.append(frm2)                            
    f3.append(frm3)                                     
fpos1=cleanup(f1)                         #calling cleanup function and add Frame1 seq to fpo1
fpos2=cleanup(f2)                         #calling cleanup function and add Frame2 seq to fpo2
fpos3=cleanup(f3)                         #calling cleanup function and add Frame3 seq to fpo3

def positions(k):                         #This funtion is to find start and stop and returns only their positions
    st=[];en=[]
    for pos in xrange(len(k)):
        if start == k[pos]: 
            st.append(pos)
        if stop1 == k[pos]:
            en.append(pos)
        if stop2 == k[pos]:
            en.append(pos)
        if stop3 == k[pos]:
            en.append(pos)
    return st,en
positions(fpos1)
positions(fpos2)
positions(fpos3)
st1=positions(fpos1)[0]                 #Here we have to change frames 123 for start 
end1=positions(fpos1)[1]                #Here we have to change frames 123 for stop 

listorf=[]
for sst in st1:                         #conditions to check for real orf's for both start and stop position 
    lst=[]
    for enn in end1:        
        len_orf=(enn)-(sst)             
        if len_orf < 267:               #It takes minimum length cutoff 
            notorf1 =(sst,enn)
            del notorf1
        else:
            notorf2 =(sst,enn)
            lst.append(sst)
            if len(lst) != 1:           #It takes only first start and stop codons and rest willbe deleted
                del notorf2
                break
            else:
                notorf3=(sst,enn)
                realorf={'start':sst,'stop':enn}  
                listorf.append(realorf)
z1=0
z2=1
while z1 < len(listorf)-1:              

    while z2 < len(listorf):
        p1=(listorf[z1])
        p2= (listorf[z2])
        strpos=p2['start']
        stppos=p1['stop']
        if stppos - strpos >0:            # condition to remove overlaping orfs 
            del listorf[z2]
        else:
            z1=z2
            z2+=1
    break

k=open('allgenes','w')
t=0
for echln in range(len(listorf)):        
    p=(listorf[echln])
    strpos=p['start']
    stppos=p['stop']+1
    fullen= fpos1[strpos:stppos]            # using regular expression cuts DNA based on positions
    sgen=''.join(fullen)
    k.write('\n'+'>'+'geneseq_frm1_'+str(t)+'\n'+str(sgen))      # writes to file with unique ID for each gene
    t+=1
k.close()

