#Script to read DNA seq and converts to a.a seq and calculates Diaminoacid Frequency_percent 

from Bio import SeqIO                  # Imported bio function to read fasta file
from Bio.Alphabet import generic_dna   # Bio python function which used to translate
import os                              # function to read from folder

#Function which converts DNA to protein seq using Bio.Alphabet
def translate_seq(seque):               
    trans = seque.translate()
    return (trans)

def freq(e):
  nLst   = ['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y','X']   #all 20 a.a and one added X was taken into a list  
  lst = {} 
  lst['A']=[];lst['C']=[];lst['D']=[];lst['E']=[];lst['F']=[];lst['G']=[];lst['H']=[];lst['I']=[];lst['K']=[];lst['L']=[];lst['M']=[];lst['N']=[];lst['P']=[];lst['Q']=[];lst['R']=[];lst['S']=[];lst['T']=[];lst['V']=[];lst['W']=[];lst['Y']=[];lst['X']=[]
  e = e.upper()
  e = e.replace("*","X")                            #converts all * to X in order to count in the list
  aaCnt= {} 
  diaaCnt= {} 
  for p in nLst:
    aaCnt[p]=0
    diaaCnt[p]={}
    for q in nLst:
      diaaCnt[p][q]=0
  aaCnt[e[0]]= 1
  diaatotal= 0
  for j in range(len(e)-1):
    p = e[j]
    q = e[j+1]
    lst[p].append(q)
    diaaCnt[p][q]+=1
    diaatotal+=1                                    #counts every added di_a.a 
    aaCnt[q]+=1
  return diaaCnt,diaatotal,aaCnt

path = "/home/praveen/Desktop/dinucleotide_new_frq/di_nucl_freq/"  # target species file name    
lists = os.listdir(path)

for files in lists:                                                # reading from the dict and output writes to new file for each input file 
    fileopen = open(path+files,'r')     
    outfl=open("Di_aafreq_out"+'_'+files,'w')
    for record in SeqIO.parse(fileopen,'fasta'):
        sequence_id = '';sequence = ''
        sequence_id += record.id;sequence += record.seq
        transeq=str(translate_seq(sequence))
    freq(transeq)
    h=freq(transeq)[0]
    c=freq(transeq)[1]
    n=freq(transeq)[2]
    
    #Function to add all di_a.a count and finds the frequncy of di_a.a_percent 
    def to_sum(ti):
        difreq_per =[]
        sumDigits = 0
        for c in ti:
            sumDigits += int(c)
        for frdi in ti:
            difreq_per.append(float(frdi)/float(sumDigits)*100)     
        return sumDigits,difreq_per
    
    totdi=[]
    totdi_aa=[]
    for f in h:
        for k in h[f]:
            nu2=[f]
            nu2.append(k)
            on2=''.join(nu2)
            totdi_aa.append(on2)
            totdi.append(float(h[f][k]))
    to_sum(totdi)                                                                    #calling function to write all freq to a new file 
    for frech in  range(len(totdi_aa)):
        print 'Di_a.a_freq_percentages_of:',totdi_aa[frech],to_sum(totdi)[1][frech]  #prints to terminal just to see
        outfl.write(str('Di_a.a_freq_percentages_of:'))
        outfl.write(str(totdi_aa[frech])+' ')
        outfl.write(str(to_sum(totdi)[1][frech])+'\n')
    outfl.write(str(n)+'\n')
outfl.close() 
