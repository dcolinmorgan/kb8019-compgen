
#function1 is to extract genes from complete genome by using information from parsing glimmer output file 
#function2 is to translate DNA seq to protein sequence

import sys
import getopt
from Tkinter import *
import glimParsPlot
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna 


object1 = glimParsPlot.parse(sys.argv[1],sys.argv[2]) 
outfl = open("protseq.txt", 'w')
jt= []
f=object1[0][2]
gl=1
def translate_seq(sequence):                 #function2
    trans = sequence.translate()
    return (trans)

for j in object1[1]:                          #function1
        lc=len(object1[1])
        start= (j[1])
        stop = (j[2])
        st1= (j[1]-1)
        stp1 = (j[2])
        st2= (j[1])
        stp2 = (j[2]-1)
        if start < stop:                       # checks condition stop codon is higher than start codon no
                kt1 = ""
                for r1 in range(st1,stp1):      
                    pt=f[r1]
                    kt1=kt1+pt
                coding_dna = Seq(kt1, generic_dna)
                tran=str(translate_seq(coding_dna))                #calling function2 here
                outfl.write('>'+'protein'+str(gl)+'\n'+tran+'\n')  #output protein seq will writes to a new file
                print '>'+'protein'+str(gl)+'\n'+tran+'\n'             
                
        if start > stop:                       # checks condition start codon no is higher than stop codon no
                kt2 =""
                for r2 in range(stp2,st2):       
                    pt2=f[r2]
                    kt2=kt2+pt2
                
                coding_dna2 = Seq(kt2, generic_dna)                  
                comp=coding_dna2.reverse_complement()            #module to reverse the seq and to complement 
                tran2=str(translate_seq(comp))                      
                outfl.write('>'+'protein'+str(gl)+'\n'+tran2+'\n')   
                print '>'+'protein'+str(gl)+'\n'+tran2+'\n'
        gl+=1

glimParsPlot.plot(object1)                      #  calling plot function from the glimparsplot 



