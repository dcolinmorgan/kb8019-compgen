from Bio.Blast import NCBIXML

w = open('xml_out', 'w+a')

result_handle = open("human_search.xml") # Now that we've got a handle we are ready to parse the output

record = NCBIXML.parse(result_handle) # Now we read the info into the 'records' object. 'Parse' because we have many records.
i = 0

records = record.next()

for alignment in records.alignments:
	title = '> ' + alignment.title + '\n'  # Isolates the annotation line in fasta
	l = alignment.hsps[0].sbjct.replace('-', '')		# We select the highest scoring sequence with the hsps[0], remove gaps ('-')
	seq = title + l + '\n'
	w.write(seq)
	

# the "sbjct" sequences are all the sequences in a target database which were compared against a "query" sequence.

# hsps = high scoring segment pairs

# parse = allows you to retrieving BLAST records one by one for each BLAST search result







