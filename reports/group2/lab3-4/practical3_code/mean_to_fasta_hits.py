# This script will disturb the orginal hit sequences from blast, before doing multiple alignment.
# The reason is to check for the effects in the resulting phylogenetic tree after multiple alignment.

w = open('mean_seq.fasta', 'w+a')

a = open('mean sequence', 'r')
b = a.readlines()
i = 0
l = 0

while i < len(b):
	if b[i][8] == '3':		# Have picked out genome 3 to have half of its genome shuffled
		l = (len(b[i+1])/2)
		k = b[i+1][l:]	  	# Second half of genome is given its own parameter
		n = k[::-1]		# Reverse second half
		x = b[i+1][0:l]   	# first half of genome is given its own parameter
		m = x+n			# Now join them into one string
		o = b[i] + m.replace('\n', '')*2	# Now "copy" the gene
		w.write(o + '\n')			# Write to w
	if b[i][0] == '>' and b[i][8] != '3':		# for the other genomes we only copy without shuffling
		d = b[i]
		f = b[i+1].replace('\n', '') + b[i+1]
		g = d+f
		w.write(g)				# Write to w
		
	i = i + 1			# Treats the next line in the input file

w.close()








		
		
