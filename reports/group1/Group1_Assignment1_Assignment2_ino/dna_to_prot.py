"""Prints the protein sequences in given DNA sequence using predicted genes
from Glimmer.

Usage:
    python dna_to_prot.py <fasta sequence file> <Glimmer predict file>
"""
import sys
import getopt

from Bio.Seq import Seq
from Bio.Alphabet import generic_dna

import glimParsPlot

def dna_2_protein(dnaseq):
    """Returns the translated sequence of given DNA sequence"""
    coding_dna = Seq(dnaseq, generic_dna)
    return str(coding_dna.translate())

def dna_2_comp_2_protein(dnaseq):
    """Returns the translated sequence of the complimentary sequence of the
    given DNA sequence."""
    coding_dna = Seq(dnaseq, generic_dna)
    return str(coding_dna.complement().translate())

def substring_glimmerindex(seq, start, stop):
    """Returns the substring in given sequence using Glimmer index values.
    Returns the reverse if start is bigger than stop."""
    if start < stop:
        return seq[start-1:stop]
    else:
        return seq[start-1:stop-2:-1]

def get_protein_seq(dnaseq, start, stop):
    """Returns the protein sequence from the given coding DNA seq."""
    if start < stop:
        return dna_2_protein(substring_glimmerindex(dnaseq, start, stop))
    else:
        return dna_2_comp_2_protein(substring_glimmerindex(dnaseq, start, stop))

def process(fasta, glimmer):
    """Prints the protein sequences in given DNA sequence using predicted genes
    from Glimmer."""
    parseobj = glimParsPlot.parse(fasta, glimmer)
    genes = parseobj[1]
    for i in range(len(genes)):
        print '>prot' + str(i)
        print get_protein_seq(parseobj[0][2], genes[i][1], genes[i][2])

def main():
    # parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
    except getopt.error, msg:
        print msg
        print "for help use --help"
        sys.exit(2)
    # process options
    for o, a in opts:
        if o in ("-h", "--help"):
            print __doc__
            sys.exit(0)
    # process arguments
    if len(args) == 2:
        process(args[0], args[1])
    else:
        print __doc__

if __name__ == "__main__":
    main()
