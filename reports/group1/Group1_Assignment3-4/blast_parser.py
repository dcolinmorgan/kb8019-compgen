'''
Comparative Genomics, Practicals 3, Part 1, by Group1 (Ino, Jessada). 
A simple biopython blast parser using the NCBIXML module

@author: Jessada Thutkawkorpin
'''
import sys, os
from optparse import OptionParser
from Bio.Blast import NCBIXML

SEQUENCE_NAME = 'sequence_name'
SEQUENCE_REF  = 'sequence_ref'

def parse(blast_xml_output):
    '''
    Input :
    - xml output from blast (filename)
    Output :
      List of homologs
    '''
    blast_records = NCBIXML.parse(open(blast_xml_output))
    result = []
    for blast_record in blast_records:
        for alignment in blast_record.alignments:
            if len(alignment.hsps) == 0:
                continue
            blast_result = {}
            tmp = str(alignment.title).split('|')
            tmp = tmp[len(tmp)-1].split()
            gene_name = tmp[len(tmp)-1].strip()
            blast_result[SEQUENCE_NAME] = '%(gene_name)s'%{"gene_name":gene_name} 
            blast_result[SEQUENCE_REF] = str(alignment.hsps[0].sbjct).replace('-', '') 
            result.append(blast_result) 
    return result


def process(blast_xml_output):
    #parse xml output from blast and print out the list of homologs
    blast_results = parse(blast_xml_output)
    for result in blast_results:
        sys.stdout.write('>%(seq_name)s\n' % {"seq_name" : result[SEQUENCE_NAME]})
        sys.stdout.write('%(seq_data)s\n' % {"seq_data" : result[SEQUENCE_REF]})
        
def main():
    usage = "%prog -x <XML output> [-h]\n" + __doc__
    
    # parse command line options
    parser = OptionParser(usage=usage)
    parser.add_option("-x", "--xml_output", dest="xml_output", 
                      type="string",
                      help="output from blast in XML format", 
                      metavar="FILE")
    
    (options, args) = parser.parse_args()
    
    # process arguments
    if not isinstance(options.xml_output, str):
        sys.stderr.write('xml output [-x] is required\n\n') 
        sys.stderr.write(parser.get_usage())
    elif not os.path.isfile(options.xml_output):
        sys.stderr.write('%(input_file)s is not a valid file name\n\n' % {"input_file" : options.xml_output}) 
        sys.stderr.write(parser.get_usage()) 
    else:
        process(options.xml_output)

if __name__ == "__main__":
    main()
