"""Calculates the average connectivity (nr of links / nr of proteins) for an
interactome. Also prints a table of degree vs. frequency.

Usage:
    python calculate_average_connectivity.py <STRING links file>
"""
import sys
import getopt

def process(linksfile):
    handler = open(linksfile, 'rU')
    proteinlinks = {}

    # Count the number of links for each protein.
    for line in handler:
        columns = line.split()

        if columns[0] in proteinlinks:
            proteinlinks[columns[0]] += 1
        else:
            proteinlinks[columns[0]] = 1

    nr_links = sum(proteinlinks.values())
    nr_proteins = len(proteinlinks)
    print 'Number of links: ' + str(nr_links)
    print 'Number of proteins: ' + str(nr_proteins)
    print 'Average connectivity: ' + str(nr_links / float(nr_proteins))

    # Calculate the degree vs frequency table.   
    deg_freqs = {}
    for k, v in proteinlinks.items():
        if v in deg_freqs:
            deg_freqs[v] += 1
        else:
            deg_freqs[v] = 1
    deg_sorted = deg_freqs.keys()
    deg_sorted.sort()

    # Print the degree vs frequency table.
    print 'Degree vs Frequencies'
    print 'Degree\tFrequency'
    for k in deg_sorted:
        print str(k) + '\t' + str(deg_freqs[k])

    #print 'Genes with frequencies between 10 and 20'
    #for k, v in proteinlinks.items():
    #    if v > 10 and v < 20:
    #        print k

def main():
    # parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
    except getopt.error, msg:
        print msg
        print "for help use --help"
        sys.exit(2)
    # process options
    for o, a in opts:
        if o in ("-h", "--help"):
            print __doc__
            sys.exit(0)
    # process arguments
    if len(args) == 1:
        process(args[0])
    else:
        print __doc__

if __name__ == "__main__":
    main()
