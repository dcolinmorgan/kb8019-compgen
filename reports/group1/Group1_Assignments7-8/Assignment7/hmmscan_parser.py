'''
Comparative Genomics, Practicals 7, Part 1, by Group1 (Ino, Jessada). 
A python script that parses the output from hmmscan to create a table showing 
the ten overall most common domains in your proteomes and ranks these domains
have in the individual proteomes

@author: Jessada Thutkawkorpin
'''
import sys, os
from optparse import OptionParser
from operator import itemgetter


def parse(hmmscan_file):
    '''
    Input :
      An output file from hmmscan
    Output :
      Frequency of each domain for that proteome
    '''
    freqs = {}
    fh = open(hmmscan_file)
    fh.next()
    fh.next()
    fh.next()
    for line in fh:
        if freqs.has_key(line[0:20].strip()):
            freqs[line[0:20].strip()] += 1
        else:
            freqs[line[0:20].strip()] = 1            
    fh.close()
    
    result = sorted(map(lambda x: x, freqs.items()), key=itemgetter(1), reverse=True)
    return result

def process(hmmscan_file):
    proteome = os.path.split(hmmscan_file)[1].split('.')[0]
    freqs = parse(hmmscan_file)
    for freq in freqs:
        sys.stdout.write('%(proteome)-20s%(domain_name)-20s : %(frequency)d\n' % {"proteome" : proteome,"domain_name" : freq[0], "frequency" : freq[1]})    

def main():
    usage = "%prog -i <hmmscan file> [-h]\n" + __doc__
    
    # parse command line options
    parser = OptionParser(usage=usage)
    parser.add_option("-i", "--input", dest="hmmscan_file", 
                      type="string",
                      help="an output file from hmmscan", 
                      metavar="FILE")
    
    (options, args) = parser.parse_args()
    
    # process arguments
    if not isinstance(options.hmmscan_file, str):
        sys.stderr.write('\nERROR!! : a genome file [-i] is required\n\n') 
        sys.stderr.write(parser.get_usage())
    elif not os.path.isfile(options.hmmscan_file):
        sys.stderr.write('\nERROR!! : %(input_file)s is not a valid file name\n\n' % {"input_file" : options.hmmscan_file}) 
        sys.stderr.write(parser.get_usage()) 
    else:
        process(options.hmmscan_file)

if __name__ == "__main__":
    main()
