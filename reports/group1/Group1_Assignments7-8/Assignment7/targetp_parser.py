'''
Comparative Genomics, Practicals 7, Part 4, by Group1 (Ino, Jessada). 
A simple python script to parse out the mitochondrial proteins

@author: Jessada Thutkawkorpin
'''
import sys, os
from optparse import OptionParser

MITOCHONDRIAL_PROTEINS = 'mitochondrial_proteins'
MITOCHONDRIAL_FRACTION = 'mitochondrial_count'

def parse(targetp_file):
    '''
    Input :
      An output file from TargetP
    Output :
      - Fraction of mitochondrial proteins
      - List of mitochondrial proteins
    '''
    
    result = {}
    result[MITOCHONDRIAL_PROTEINS] = []
    result[MITOCHONDRIAL_FRACTION] = float(0)
    
    count = 0
    fh = open(targetp_file)
    for line in fh:
        if line[0:7] != 'genome_':
            continue        
        count += 1
        if line[57:58] == 'M':
            result[MITOCHONDRIAL_FRACTION]    += 1
            result[MITOCHONDRIAL_PROTEINS].append(line[0:17].strip())
    result[MITOCHONDRIAL_FRACTION] /= count             
    fh.close()
    
    return result

def process(targetp_file):
    result = parse(targetp_file)
    sys.stdout.write('Fraction of mitochondrial proteins : %(fraction)4.4f\n' % {"fraction" : result[MITOCHONDRIAL_FRACTION]})
    for item in result[MITOCHONDRIAL_PROTEINS]:
        sys.stdout.write('%(protein)s\n' % {'protein': item})

def main():
    usage = "%prog -i <targetp output> [-h]\n" + __doc__
    
    # parse command line options
    parser = OptionParser(usage=usage)
    parser.add_option("-i", "--input", dest="targetp_file", 
                      type="string",
                      help="an output from TargetP", 
                      metavar="FILE")
    
    (options, args) = parser.parse_args()
    
    # process arguments
    if not isinstance(options.targetp_file, str):
        sys.stderr.write('\nERROR!! : a TargetP output file [-i] is required\n\n') 
        sys.stderr.write(parser.get_usage())
    elif not os.path.isfile(options.targetp_file):
        sys.stderr.write('\nERROR!! : %(input_file)s is not a valid file name\n\n' % {"input_file" : options.targetp_file}) 
        sys.stderr.write(parser.get_usage()) 
    else:
        process(options.targetp_file)

if __name__ == "__main__":
    main()
