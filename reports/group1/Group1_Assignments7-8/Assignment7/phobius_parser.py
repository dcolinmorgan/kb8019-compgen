'''
Comparative Genomics, Practicals 7, Part 3, by Group1 (Ino, Jessada). 
A simple python script to parse the output file from phobius to retrieve 
the following data
 - The fraction of proteins with 0 TM segments.
 - The fraction of proteins with > 0 TM segments.
 - The average number of TM segments for those with > 0 segments.
 - The fraction of proteins with > 0 signal peptide.
 - The fraction of those (with > 0 signal peptide) with > 0 TM segment.

@author: Jessada Thutkawkorpin
'''
import sys, os
from optparse import OptionParser

FRACTION_ZERO_TM_SEGMENTS              = 'fraction_zero_TM_segments'
FRACTION_NONZERO_TM_SEGMENTS           = 'fraction_nonzero_TM_segments'
AVERAGE_NONZERO_TM_SEGMENTS            = 'average_nonzero_TM_segments'
FRACTION_NONZERO_SIGNAL_PEPTIDE        = 'fraction_nonzero_signal_peptide'
FRACTION_NONZERO_TM_AND_SIGNAL_PEPTIDE = 'fraction_nonzero_TM_and_signal_peptide'

def parse(phobius_file):
    '''
    Input :
      An output from phobius
    Output :
      - The fraction of proteins with 0 TM segments. 'fraction_zero_TM_segments'
      - The fraction of proteins with > 0 TM segments. 'fraction_nonzero_TM_segments'
      - The average number of TM segments for those with > 0 segments. 'average_nonzero_TM_segments'
      - The fraction of proteins with > 0 signal peptide. 'fraction_zero_signal_peptide'
      - The fraction of those (with > 0 signal peptide) with > 0 TM segment. 'fraction_nonzero_TM_and_signal_peptide'
    '''
    
    result = {}
    result[FRACTION_ZERO_TM_SEGMENTS]              = float(0)
    result[FRACTION_NONZERO_TM_SEGMENTS]           = float(0)
    result[AVERAGE_NONZERO_TM_SEGMENTS]            = float(0)
    result[FRACTION_NONZERO_SIGNAL_PEPTIDE]        = float(0)
    result[FRACTION_NONZERO_TM_AND_SIGNAL_PEPTIDE] = float(0)
    
    fh = open(phobius_file)
    fh.next()
    count = 0
    for line in fh:
        TM = int(line[31:33])
        SP = str(line[35:36])
        if TM == 0:
            result[FRACTION_ZERO_TM_SEGMENTS] += 1
        else:                        
            result[FRACTION_NONZERO_TM_SEGMENTS] += 1
            result[AVERAGE_NONZERO_TM_SEGMENTS] += TM
        if SP == 'Y':
            result[FRACTION_NONZERO_SIGNAL_PEPTIDE] += 1
            if TM > 0:
                result[FRACTION_NONZERO_TM_AND_SIGNAL_PEPTIDE] += 1
        count += 1
    
    result[AVERAGE_NONZERO_TM_SEGMENTS]            /= result[FRACTION_NONZERO_TM_SEGMENTS]
    result[FRACTION_NONZERO_TM_SEGMENTS]           /= count
    result[FRACTION_ZERO_TM_SEGMENTS]              /= count
    result[FRACTION_NONZERO_TM_AND_SIGNAL_PEPTIDE] /= result[FRACTION_NONZERO_SIGNAL_PEPTIDE]
    result[FRACTION_NONZERO_SIGNAL_PEPTIDE]        /= count
    fh.close()
    
    return result

def process(phobius_file):
    result = parse(phobius_file)
    for key in result:
        sys.stdout.write('%(key)-40s : %(result)4.4f\n' % {"key" : key, "result" : result[key]})
    '''
    proteome = os.path.split(phobius_file)[1].split('.')[0]
    freqs = parse(phobius_file)
    for freq in freqs:
        sys.stdout.write('%(proteome)-20s%(domain_name)-20s : %(frequency)d\n' % {"proteome" : proteome,"domain_name" : freq[0], "frequency" : freq[1]})
    '''    

def main():
    usage = "%prog -i <phobius output> [-h]\n" + __doc__
    
    # parse command line options
    parser = OptionParser(usage=usage)
    parser.add_option("-i", "--input", dest="phobius_file", 
                      type="string",
                      help="output from phobius", 
                      metavar="FILE")
    
    (options, args) = parser.parse_args()
    
    # process arguments
    if not isinstance(options.phobius_file, str):
        sys.stderr.write('\nERROR!! : a phobius output [-i] is required\n\n') 
        sys.stderr.write(parser.get_usage())
    elif not os.path.isfile(options.phobius_file):
        sys.stderr.write('\nERROR!! : %(input_file)s is not a valid file name\n\n' % {"input_file" : options.phobius_file}) 
        sys.stderr.write(parser.get_usage()) 
    else:
        process(options.phobius_file)

if __name__ == "__main__":
    main()
