"""Calculates frequencies in protein sequences and nucleotide sequences for
window sizes 1 and 2.

Usage:
    python calculate_frequencies.py <fastafile1> <fastfilen>
Options:
    -p     The fasta file contains protein sequences.
    -o     Stores a picklefile of the calculated frequencies which can be used
           as input for calculate_distance_matrix.py
"""
import sys
import getopt
import pickle

from Bio import SeqIO

class Genome:
    """Stores frequencies for genomes"""
    def __init__(self, acc, length, num_n, num_din, freqs, difreqs):
        self.acc = acc
        self.length = length
        self.num_n = num_n
        self.num_din = num_din
        self.freqs = freqs
        self.difreqs = difreqs

    def get_acc(self):
        return self.acc

    def get_length(self):
        return self.length

    def get_num_n(self):
        return self.num_n

    def get_num_din(self):
        return self.num_din

    def get_freq_keys(self):
        return self.freqs.keys()

    def get_difreq_keys(self):
        return self.difreqs.keys()
    
    def get_freqs(self):
        return self.freqs

    def get_difreqs(self):
        return self.difreqs

    def get_freq(self, key):
        if key in self.freqs:
            return self.freqs[key]
        else:
            return 0

    def get_difreq(self, key):
        if key in self.difreqs:
            return self.difreqs[key]
        else:
            return 0

    def get_gcc(self):
        return (self.get_freq('G') + self.get_freq('C')) / \
               float( \
                       self.get_freq('A') + \
                       self.get_freq('C') + \
                       self.get_freq('G') + \
                       self.get_freq('T')) * 100

class Proteome:
    """Stores frequencies for proteomes"""
    def __init__(self, acc, length, num_n, num_din, freqs, difreqs):
        self.acc = acc
        self.length = length
        self.num_n = num_n
        self.num_din = num_din
        self.freqs = freqs
        self.difreqs = difreqs

    def get_acc(self):
        return self.acc

    def get_length(self):
        return self.length

    def get_num_n(self):
        return self.num_n

    def get_num_din(self):
        return self.num_din

    def get_freq_keys(self):
        return self.freqs.keys()

    def get_difreq_keys(self):
        return self.difreqs.keys()

    def get_freqs(self):
        return self.freqs

    def get_difreqs(self):
        return self.difreqs

    def get_freq(self, key):
        if key in self.freqs:
            return self.freqs[key]
        else:
            return 0

    def get_difreq(self, key):
        if key in self.difreqs:
            return self.difreqs[key]
        else:
            return 0

def compute_gcc(num_a, num_c, num_g, num_t):
    return (num_g + num_c) / float(num_a + num_c + num_g + num_t) * 100

def compute_aa_frequencies(sequence):
    return compute_frequencies(sequence, True)

def compute_n_frequencies(sequence):
    return compute_frequencies(sequence, False)

def compute_frequencies(acc, sequence, isprotein):
    num_n = 0
    num_din = 0
    freqs = {}
    difreqs = {}

    for i in range(len(sequence)):
        n = str(sequence[i])
        n.upper()

        if not isprotein and n != 'N' or (isprotein and n != 'X' and n != '*'):
            num_n += 1

            if n in freqs:
                freqs[n] += 1
            else:
                freqs[n] = 1

        if i < len(sequence) - 1:
            din = str(sequence[i:i+2])
            din.upper()

            if not isprotein and 'N' not in din or (isprotein and '*' not in din and 'X' not in din):
                num_din += 1

                if din in difreqs:
                    difreqs[din] += 1
                else:
                    difreqs[din] = 1

    for k in freqs.keys():
        freqs[k] /= float(num_n)
    for k in difreqs.keys():
        difreqs[k] /= float(num_din)

    if not isprotein:
        return Genome(acc, len(sequence), num_n, num_din, freqs, difreqs)
    else: 
        return Proteome(acc, len(sequence), num_n, num_din, freqs, difreqs)

def process(fastafiles, isprotein, picklefile):
    records = []
    for fastafile in fastafiles:
        handle = open(fastafile, "rU")
        records.extend(list(SeqIO.parse(handle, "fasta")))
        handle.close()

    # Compute frequencies for the given records
    computed_frequencies = []
    for record in records:
        computed_frequencies.append(compute_frequencies(record.id, record.seq,
            isprotein))

    # Print sequence names
    for cf in computed_frequencies:
        sys.stdout.write('\t' + cf.get_acc())
    sys.stdout.write('\n')

    # Print general information
    sys.stdout.write('Length')
    for cf in computed_frequencies:
        sys.stdout.write('\t' + str(cf.get_length()))
    sys.stdout.write('\n')
    sys.stdout.write('Total Mono Freq')
    for cf in computed_frequencies:
        sys.stdout.write('\t' + str(cf.get_num_n()))
    sys.stdout.write('\n')
    sys.stdout.write('Total Di Freq')
    for cf in computed_frequencies:
        sys.stdout.write('\t' + str(cf.get_num_din()))
    sys.stdout.write('\n')

    # Print frequencies
    if not isprotein:
        sys.stdout.write('GC Content')
        for cf in computed_frequencies:
            sys.stdout.write('\t' + str(cf.get_gcc()))
        sys.stdout.write('\n')
        for c in 'ACGT':
            sys.stdout.write(c)
            for cf in computed_frequencies:
                sys.stdout.write('\t' + str(cf.get_freq(c)))
            sys.stdout.write('\n')
        for c1 in 'ACGT':
            for c2 in 'ACGT':
                sys.stdout.write(c1 + c2)
                for cf in computed_frequencies:
                    sys.stdout.write('\t' + str(cf.get_difreq(c1 + c2)))
                sys.stdout.write('\n')
    else:
        for c in 'ARNDCQEGHILKMFPSTWYV':
            sys.stdout.write(c)
            for cf in computed_frequencies:
                sys.stdout.write('\t' + str(cf.get_freq(c)))
            sys.stdout.write('\n')
        for c1 in 'ARNDCQEGHILKMFPSTWYV':
            for c2 in 'ARNDCQEGHILKMFPSTWYV':
                allzero = True
                for cf in computed_frequencies:
                    if cf.get_difreq(c1 + c2) != 0:
                        allzero = False
                if not allzero:
                    sys.stdout.write(c1 + c2)
                    for cf in computed_frequencies:
                        sys.stdout.write('\t' + str(cf.get_difreq(c1 + c2)))
                    sys.stdout.write('\n')

    if picklefile is not None:
        pickle.dump(computed_frequencies, open(picklefile, "wb"))

def main():
    # parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hpo:", ["help"])
    except getopt.error, msg:
        print msg
        print "for help use --help"
        sys.exit(2)
    # process options
    isprotein = False
    picklefile = None
    for o, a in opts:
        if o in ("-h", "--help"):
            print __doc__
            sys.exit(0)
        elif o == '-p':
            isprotein = True
        elif o == '-o':
            picklefile = a
    # process arguments
    if len(args) > 0:
        process(args, isprotein, picklefile)
    else:
        print __doc__

if __name__ == "__main__":
    main()
