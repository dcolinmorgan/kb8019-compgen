'''
Comparative Genomics, Project assignment,  by Group1 (Ino, Jessada). 
ORF Finder : compute Open Reading Frames (ORFs) in a genome

@author: Jessada Thutkawkorpin
'''

import sys, os
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from optparse import OptionParser

ORF_NAME     = 'orf_name'
FIRST_INDEX  = 'first_index'
LAST_INDEX   = 'last_index'
ORF_SEQUENCE = 'orf_sequence'

START_CODON  = 'ATG'
STOP_CODON   = ['TAG', 'TAA', 'TGA']

READING_FRAME = 3

MIN_ORF_SIZE  = 60

def rv_index(fw_idx, seq_len):
    return seq_len - fw_idx - 1

def search(genome_file):
    '''
    Input :
      A genome file in FASTA format
    Output :
      List of entries for each of the ORF gene sequences, with unique names
    '''
    
    orfs = []
    
    #Indexing the next stop codon for each nucleotide in each reading frame in the forward direction
    last_stop_codons = {0:0, 1:1, 2:2}
    seq_record = SeqIO.read(open(genome_file), "fasta")
    next_stop_codon_index = {}
    for i in xrange(len(seq_record.seq)):
        next_stop_codon_index[i] = None
    for i in xrange(len(seq_record.seq)-2):
        if str(seq_record.seq[i:i+READING_FRAME]) in STOP_CODON:
            if i - last_stop_codons[i%3] < MIN_ORF_SIZE: 
                last_stop_codons[i%3] = i
                continue
            for j in xrange(last_stop_codons[i%3],i,READING_FRAME):
                next_stop_codon_index[j] = i                
            last_stop_codons[i%3] = i
    
    #Finding forward orf using stop codon index from above
    for i in xrange(len(seq_record.seq)-2):
        if str(seq_record.seq[i:i+READING_FRAME]) != START_CODON:
            continue
        if next_stop_codon_index[i+READING_FRAME] == None:
            continue
        if next_stop_codon_index[i+READING_FRAME] - i < MIN_ORF_SIZE:
            continue
        orf = {ORF_NAME:'orf%(orf_count)03d' % {'orf_count':len(orfs)}, FIRST_INDEX:i, LAST_INDEX:next_stop_codon_index[i+READING_FRAME]+2, ORF_SEQUENCE: str(seq_record.seq[i:next_stop_codon_index[i+READING_FRAME]+3])}
        orfs.append(orf)
    
    #Indexing the next stop codon for each nucleotide in each reading frame in the backward direction
    seq_len = len(seq_record.seq)
    last_stop_codons = {0:0, 1:1, 2:2}
    reverse_complement_seq = seq_record.seq.reverse_complement()
    next_stop_codon_index = {}
    for i in xrange(len(reverse_complement_seq)):
        next_stop_codon_index[i] = None
    for i in xrange(len(reverse_complement_seq)-2):
        if str(reverse_complement_seq[i:i+READING_FRAME]) in STOP_CODON:
            if i - last_stop_codons[i%3] < MIN_ORF_SIZE: 
                last_stop_codons[i%3] = i
                continue
            for j in xrange(last_stop_codons[i%3],i,READING_FRAME):
                next_stop_codon_index[j] = i                
            last_stop_codons[i%3] = i
        
    #Finding backward orf using stop codon index from above
    for i in xrange(len(seq_record.seq)-2):
        if str(reverse_complement_seq[i:i+READING_FRAME]) != START_CODON:
            continue
        if next_stop_codon_index[i+READING_FRAME] == None:
            continue
        if next_stop_codon_index[i+READING_FRAME] - i < MIN_ORF_SIZE:
            continue
        orf = {ORF_NAME:'orf%(orf_count)03d' % {'orf_count':len(orfs)}, FIRST_INDEX:rv_index(i, seq_len), LAST_INDEX:rv_index(next_stop_codon_index[i+READING_FRAME], seq_len)-2, ORF_SEQUENCE: str(reverse_complement_seq[i:next_stop_codon_index[i+READING_FRAME]+3])}
        orfs.append(orf)
        
    return orfs

def get_concatenated_orf_protein(genome_file):
    result = ''
    orfs = search(genome_file)
    for orf in orfs:
        coding_dna = Seq(str(orf[ORF_SEQUENCE]), IUPAC.ambiguous_dna)
        result += str(coding_dna.translate())
    
    return result

def process(genome_file):
    orfs = search(genome_file)
    for orf in orfs:
        sys.stdout.write('>%(orf_name)s|%(first_index)s|%(last_index)s\n' % {"orf_name" : orf[ORF_NAME], "first_index" : orf[FIRST_INDEX], "last_index" : orf[LAST_INDEX]})
        sys.stdout.write('%(orf_sequence)s\n' % {"orf_sequence" : orf[ORF_SEQUENCE]})

def main():
    usage = "%prog -i <genome file> [-h]\n" + __doc__
    
    # parse command line options
    parser = OptionParser(usage=usage)
    parser.add_option("-i", "--input", dest="genome_file", 
                      type="string",
                      help="a genome file in FASTA format", 
                      metavar="FILE")
    
    (options, args) = parser.parse_args()
    
    # process arguments
    if not isinstance(options.genome_file, str):
        sys.stderr.write('\nERROR!! : a genome file [-i] is required\n\n') 
        sys.stderr.write(parser.get_usage())
    elif not os.path.isfile(options.genome_file):
        sys.stderr.write('\nERROR!! : %(input_file)s is not a valid file name\n\n' % {"input_file" : options.genome_file}) 
        sys.stderr.write(parser.get_usage()) 
    else:
        process(options.genome_file)

if __name__ == "__main__":
    main()
