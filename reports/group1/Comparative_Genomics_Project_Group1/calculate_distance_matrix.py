"""Calculates a distance matrix using frequencies of nucleotides or aminoacids
in protein sequences or nucleotide sequences.

Usage:
    python <options> calculate_distance_matrix.py <multifasta file>
    python <options> calculate_distance_matrix.py <fastafile1> <fastafilen>
Options:
    -p     The fasta file contains protein sequences.
    -n     Normalize
    -s x   Scale with given factor
    -i x   Use pickle file with precalculated frequencies
    -d q   Square distance
       a   Angle distance
    -c m   Monofrequencies
       d   Difrequencies
       md  Monodifrequencies
       g   GC Content (Only for DNA sequences)
"""
import sys
import getopt
import math
import pickle

from Bio import SeqIO

import calculate_frequencies
from calculate_frequencies import Genome, Proteome

def compute_square_distance(freqs1, freqs2):
    square_differences = 0
    for k1, v1 in freqs1.items():
        if k1 in freqs2:
            square_differences += (freqs2[k1] - v1)**2
        else:
            square_differences += v1**2

    return math.sqrt(square_differences)

def compute_abs_distance(freqs1, freqs2):
    total = 0
    for k1, v1 in freqs1.items():
        if k1 in freqs2:
            total += abs(freqs2[k1] - v1)
        else:
            total += v1

    return total

def compute_angle(freqs1, freqs2):
    norm1 = 0
    for v in freqs1.values():
        norm1 += v**2
    norm1 = math.sqrt(norm1)

    norm2 = 0
    for v in freqs2.values():
        norm2 += v**2
    norm2 = math.sqrt(norm2)

    dotp = 0
    for k in set(freqs1.keys() + freqs2.keys()):
        if k in freqs1 and k in freqs2:
            dotp += (freqs1[k] / norm1) * (freqs2[k] / norm2)
        else:
            dotp = 0

    return math.acos(dotp)

def print_distance_matrix(dist_matrix):
    for i in range(len(dist_matrix)):
        for j in range(len(dist_matrix[i])):
            sys.stdout.write(str(dist_matrix[i][j]) + '\t')
        sys.stdout.write('\n')

def get_freq(freqs, key):
    if key in freqs:
        return freqs[key]
    else:
        return 0

def calculate_distance_matrix(records, isprotein, comparing, distance, normalize,
        scale, inputpicklefile):
    # Use the picklefile if available to prevent calculating the frequencies
    # again.
    if inputpicklefile == None:
        computed_frequencies = []

        for record in records:
            rv = calculate_frequencies.compute_frequencies(record.id, record.seq, isprotein)
            computed_frequencies.append(rv)
    else:
        computed_frequencies = pickle.load(open(inputpicklefile))

    # Initialize matrix
    dist_matrix = []
    for i in range(len(computed_frequencies)):
        dist_matrix.append([])
        for j in range(len(computed_frequencies)):
            dist_matrix[i].append(-1)

    # Calculate matrix
    if normalize:
        largest_value = 0
    for i in range(len(computed_frequencies)):
        for j in range(i, len(computed_frequencies)):
            if i == j:
                dist_matrix[i][j] = 0
            else:
                # Monofrequencies
                if comparing == 'm':
                    dic_i = computed_frequencies[i].get_freqs()
                    dic_j = computed_frequencies[j].get_freqs()
                # Difrequencies
                elif comparing == 'd':
                    dic_i = computed_frequencies[i].get_difreqs()
                    dic_j = computed_frequencies[j].get_difreqs()
                # Mono and Difrequencies
                elif comparing == 'md':
                    dic_i = dict(computed_frequencies[i].get_freqs().items() + \
                                 computed_frequencies[i].get_difreqs().items())
                    dic_j = dict(computed_frequencies[j].get_freqs().items() + \
                                 computed_frequencies[j].get_difreqs().items())
                # GC Content
                elif comparing == 'g':
                    dic_i = {}
                    dic_j = {}
                    dic_i['GC'] = computed_frequencies[i].get_gcc()
                    dic_j['Gc'] = computed_frequencies[j].get_gcc()
                else:
                    sys.stderr.write('Invalid comparison character')
                    return

                # Square distance
                if distance == 'q':
                    dist_matrix[i][j] = compute_square_distance(dic_i, dic_j)
                # Angle distance
                elif distance == 'a':
                    dist_matrix[i][j] = compute_angle(dic_i, dic_j)
                else:
                    sys.stderr.write('Invalid distance character')
                    return
                
                # Keep track of the largest value
                if normalize and dist_matrix[i][j] > largest_value:
                    largest_value = dist_matrix[i][j]

                # The matrix is diagonally mirrored
                dist_matrix[j][i] = dist_matrix[i][j]

    # Normalize
    if normalize:
        largest_value = float(largest_value)

        for i in range(len(computed_frequencies)):
            for j in range(i, len(computed_frequencies)):
                    dist_matrix[i][j] /= largest_value
                    dist_matrix[j][i] = dist_matrix[i][j]

    # Scale
    if scale != None:
        for i in range(len(computed_frequencies)):
            for j in range(i, len(computed_frequencies)):
                    dist_matrix[i][j] *= scale
                    dist_matrix[j][i] = dist_matrix[i][j]


    # Add column names
    dist_matrix.insert(0, [])
    for cf in computed_frequencies:
        dist_matrix[0].append(cf.get_acc())

    return dist_matrix
    

def process(fastafiles, isprotein, comparing, distance, normalize, scale, inputpicklefile):
    records = []

    # If there is a picklefile, reading the input files is not necessary
    if inputpicklefile == None:
        for fastafile in fastafiles:
            handle = open(fastafile, "rU")
            records.extend(list(SeqIO.parse(handle, "fasta")))
            handle.close()

    print_distance_matrix(calculate_distance_matrix(records, isprotein,
        comparing, distance, normalize, scale, inputpicklefile))

def main():
    # parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "d:c:s:i:hnp", ["help"])
    except getopt.error, msg:
        print msg
        print "for help use --help"
        sys.exit(2)
    # process options
    isprotein = False
    normalize = False
    comparing = 'm'
    distance = 'q'
    inputpicklefile = None
    scale = None
    for o, a in opts:
        if o in ("-h", "--help"):
            print __doc__
            sys.exit(0)
        elif o == '-p':
            isprotein = True
        elif o == '-n':
            normalize = True
        elif o == '-s':
            scale = float(a)
        elif o == '-i':
            inputpicklefile = a
        elif o == '-d':
            if a == 'q' or a == 'a':
                distance = a
            else:
                print __doc__
                sys.exit(0)
        elif o == '-c':
            if a == 'm' or a == 'd' or a == 'md' or a == 'g':
                comparing = a
            else:
                print __doc__
                sys.exit(0)

    # process arguments
    if len(args) >= 1:
        process(args, isprotein, comparing, distance, normalize, scale, inputpicklefile)
    else:
        print __doc__

if __name__ == "__main__":
    main()
