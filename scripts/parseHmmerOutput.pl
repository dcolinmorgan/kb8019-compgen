open (FH, $ARGV [0]);

$mode = 0;

while (<FH>) {

 $aLine = $_;
 chomp ($aLine);

 if ($aLine =~ /Query sequence: (\S*)/) {

  $ac = $1;
  @doms = ();

 }

 if ($aLine =~ /Parsed for domains:/) {

  $mode = 1;

 }

 if ($aLine =~ /^(PF\d{5})/ && $mode == 1) {

  push (@doms, $1);

 }

 if ($aLine =~ /^\/\//) {

  $mode = 0;  

  $sS = "$ac\t";

  foreach $aDomain (@doms) {

   $sS = $sS."$aDomain*";

  }

  if (@doms > 0) {

   chop ($sS);

  }

  $sS = $sS."\n";

  print "$sS";

 }

}

close (FH);